<?php include("header.php"); ?>
<section class="container breadcrumb mobile-none">
	<ul>
		<li><a href="/">Strona główna</a></li>
		<li><a href="/events.php">Wyszukiwanie</a></li>
		<li>Wydarzenie</li>
	</ul>
</section>
<section class="event-intro" style="background: url(http://lorempixel.com/1800/600/?rnd=<?php echo rand(); ?>)no-repeat;">
 	<div class="event-title">
 				<div class="col-md-12 col-sm-12 col-xs-12 mobile-show">
 				<div class="event-social-icons">
					<div class="col-lg-6 col-md-12 col-xs-12 mobile-show" >        				 
	        				 <div class="pull-left">
								<div class="platform facebook"></div>
								<div class="platform instagram"></div>
								<div class="platform yelp"></div>
								<!-- facebook, yelp, instagram -->	
	        				 </div>
	        				 <div class="faces pull-right">
								<div class="face" style="background:url(images/fake-face1.png) center center no-repeat; background-size:cover;"></div>
								<div class="face" style="background:url(images/fake-face2.png) center center no-repeat; background-size:cover;"></div>
								<div class="amount">+50</div>
							</div>
	        			</div>
 				</div>
			</div>
 		<div class="container"> 	
 			<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
 				<div class="event-title-mob">
 					<h1>Madonna World Tour I am sexy girl of the millenium</h1>
					<div class="address">Klub Plan B, Warszawa, Narbutta 27</div>
 				</div> 			
			</div> 	
			<div class="col-lg-4 mobile-none noprint">
 				<div class="like-event">
					<div class="heart"></div>
					<div class="text">Polub</div>
				</div>
			</div>		
 		</div>
	</div>
</section> 
<div class="container event-description">
	<div class="row">
		<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
			<div class="col-lg-12 col-md-12 col-xs-12"> 
				<div class="description">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa. Etiam vehicula velit a libero convallis, a aliquam lacus vehicula. Quisque malesuada fermentum turpis, eget maximus lectus vulputate at. Etiam vitae ultrices diam, a sodales sem. Proin auctor ligula quis metus condimentum, ac tempor neque imperdiet. Donec sit amet tellus neque. Fusce ultricies metus ut nulla malesuada tincidunt. Nullam quis velit ut neque sagittis convallis ac a mi. Proin turpis metus, faucibus ullamcorper sollicitudin malesuada, vestibulum in justo. Maecenas et orci sed mi fringilla aliquet. Morbi eget sapien a mauris consectetur scelerisque vitae at urna. Proin et est tincidunt, rutrum magna in, eleifend tortor.
				</div> 
				<div id="event-more">Więcej</div>
			</div>
			
			<div class="col-lg-12 col-md-12 col-xs-12">
				<div class="links">				
					<?php
						function skroc($tekst, $ilosc_znakow) { //SKRACA CIĄG DO PODANEJ DŁUGOŚCI
						return substr($tekst, 0, $ilosc_znakow).'...';
						} 
					?>
					<a href="#" class="trackback"><?php echo(skroc("http://www.ticketpro.pl/jnp/muzyka/42344543-nowe-sytuacje-group.html", 40)."<br>"); ?></a>
					<a href="#" class="facebook"><?php echo(skroc("http://www.ticketpro.pl/jnp/muzyka/42344543-nowe-sytuacje-group.html", 40)."<br>"); ?></a>	
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-xs-12">
				<div class="buy-tickets">
					<div class="date-price">
						<span class="pull-left">Data: 14.06.2015</span>
						<span class="pull-right">Cena biletu: 55 zł</span>
					</div>
				</div>
			</div>		
			<div class="col-lg-6 col-md-12 col-xs-12">				
				<div class="order-food">
					<button class="btn btn-red btn-fix btn-order-food centered" type="button" data-link="http://evenea.pl">kup bilet</button>
				</div>			 			 
			</div>
			<div class="col-lg-6 col-md-12 col-xs-12 noprint" style="margin-top:10px;">
				<a href="#" class="pull-right" data-modal="modal-report-poi">Zgłoś</a>
			</div>
				<div class="col-lg-12 col-md-12 col-xs-12">
	 				<div class="photo">
						<h2>Zdjęcia</h2>				
						<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-1.png" border="0" /></a></div>
						<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-2.png" border="0" /></a></div>
						<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-3.png" border="0" /></a></div>
						<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-4.png" border="0" /></a></div>
					</div>
				</div> 
			</div> <!--col-lg-7-->
		<div class="col-lg-5 col-md-12 col-sm-12 col-sx-12 noprint">
			<div class="row people mobile-none noprint">
				<div class="col-lg-5">
					Ty i 10 znajomych idzie
					<br />
					W sumie 234 osoby
				</div>
				<div class="col-lg-7 col-md-12 col-sm-12 col-sx-12">
					<div class="faces">
						<div class="face" style="background:url(images/fake-face1.png) center center no-repeat; background-size:cover;"></div>
						<div class="face" style="background:url(images/fake-face2.png) center center no-repeat; background-size:cover;"></div>
						<div class="face" style="background:url(images/fake-face1.png) center center no-repeat; background-size:cover;"></div>
						<div class="face" style="background:url(images/fake-face2.png) center center no-repeat; background-size:cover;"></div>
						<div class="amount">+10</div>
					</div>
				</div>
			</div>		
			<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 noprint">
				<div class="buttons">
					<button class="event-action active">Idę</button>
					<button class="event-action">Może</button>
					<button class="event-action">Nie idę</button>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 noprint">				
				<div class="share">				  
					<p>Poleć nas</p>
					<a href="#" class="share-facebook fb" data-desc="Trattoria Toscana - Kuchnia Śródziemnomorska" data-img="http://kolomnie.farandwide.pl/images/fake.jpg" data-url="http://kolomnie.farandwide.pl"></a>
					<a href="#" class="share-twitter tw" data-url="http://kolomnie.farandwide.pl" data-desc="Trattoria Toscana - Kuchnia Śródziemnomorska"></a>
					<a href="#" class="share-google gp" data-url="http://kolomnie.farandwide.pl"></a>
					<a href="#" class="mail" data-modal="modal-invite"></a>
				</div>	
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-sx-12 mobile-none noprint">
				<h2>Co nowego na Facebooku</h2>				
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&appId=828532820530910&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like-box"  data-href="https://www.facebook.com/MultikinoPolska?fref=ts" data-width="430"  data-stream="true" data-show-border="false" data-show-faces="false" data-header="false"></div>
			</div>				
		</div>
	</div><!--row-->
 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noprint">
 			<h2>Wydarzenia podobne</h2>
 		</div>
 		<div class="row noprint">
			<?php include("event-box.php"); ?>
			<?php include("event-box.php"); ?>
			<?php include("event-box.php"); ?>
 		</div>			
			
	 
</div><!--container-->

<script>
$(document).ready(function() {
	var descriptionHeight = $('.event-description .description').height();
	if(descriptionHeight >= 115){
		$('.event-description .description').css({height: '115px'});
		$('#event-more').show();
	}	
	$('#event-more').click(function(){
		$('.event-description .description').css({height: 'auto'}); 
		$('#event-more').hide();
	});
});
</script>
<?php include("footer.php"); ?>

