<?php include("header.php"); ?> 
<main class="cd-main-content">
	<div class="main-page map-page">
		<div id="map-big"></div>
	</div>
</main>

<!--js-->
<script type="text/javascript">
	var points = [
		{
			name:'Lorem ipsum 1',
			lat:0,
			lon:0,
			address: 'Warszawa, Gołębiowskiego 4',
			stars:1,
			img: 'images/fake.jpg'
		},
		{
			name:'Lorem ipsum 2',
			lat:10,
			lon:-10,
			address: 'Warszawa, Gołębiowskiego 4',
			stars:2,
			img: 'images/fake.jpg'
		},
		{
			name:'Lorem ipsum 3',
			lat:20,
			lon:30,
			address: 'Warszawa, Gołębiowskiego 4',
			stars:3,
			img: 'images/fake.jpg'
		}
	];
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();  
	var map = null;
	function initialize() {
		var mapOptions = {
			center: { lat: -34.397, lng: 150.644},
			zoom: 8
		};
		map = new google.maps.Map(document.getElementById('map-big'), mapOptions);
		for(var i=0; i<points.length; i++){
			myLatlng = new google.maps.LatLng(points[i].lat,points[i].lon);
			marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				title: points[i].name,
				icon: 'images/lokalizacja_ikona.png'
			});
			bounds.extend(marker.position);
			google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				return function() {
					infowindow.setContent('<table><tr><td valign="top"><div style="background:url('+points[i].img+') center center no-repeat;background-size:cover;width:100px;height:100px;" /></td><td width="10"></td><td valign="top"><div style="font-weight:600;font-size:15px;">'+points[i].name+'</div><div style="color:#8B8B8B;">'+points[i].address+'</div><div class="stars stars-'+points[i].stars+'"></div></td></table>');
					infowindow.open(map, marker);
				}
			})(marker, i));
		}
		map.fitBounds(bounds);
		getLocation();
	}
	function addUserMarker(position){ 
		var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: 'Twoje położenie',
			icon: 'images/user_position_icon.png'
		});
		bounds.extend(marker.position);
		map.fitBounds(bounds);
	}
	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(addUserMarker);
		}
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php include("end.php"); ?>

