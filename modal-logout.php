<div id="modal-logout" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Wyloguj się</h1>
		<div class="info">
			Hej! Czy na pewno chcesz się wylogować? 
			<br />
			Niektóre usługi mogą być niedostępne.
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<button class="button-blue">Potwierdź</button>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
				<button class="button-red modal-close">Anuluj</button>
			</div>
		</div>
	</div>
</div>