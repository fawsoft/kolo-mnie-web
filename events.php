<?php include("header.php"); ?>

<section class="events-breadcrumb">
	<div class="container">
		<div class="mobile-none"><a href="/">Powrót do wyników wyszukiwania</a></div>
		<div class="filter-mob-select mobile-show">
			<select class="filtry-mob">
				<option value="distance">najbliżej</option>
				<option value="popularity">najpopularniejsze</option>
				<option value="rating">najwyżej oceniane</option>
			</select>	
		</div>
	</div>
</section>

<?php include("filter-bar-events.php"); ?>

<section class="container">
	<div id="promos-container" class="row"></div>
	<div id="loadmoreajaxloader" style="display:none;"><center><img src="images/loader-infinite.gif" /></center></div>
</section>

<script>
$(document).ready(function() {
	vars.infiniteScroll.type = 'events';
	reloadPlaces();
});
$(window).scroll(function(){
    infiniteScroll();
});
</script>

<?php include("footer.php"); ?>

