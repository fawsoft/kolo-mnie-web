<div id="modal-invite" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Zaproś znajomych</h1>
		<div class="buttons center">
			<a href="#"><img src="images/fb-invite.png" border="0" /></a>
			<div class="or">
				lub przez email
			</div>
			<div>
				<input id="invite-email" type="email" placeholder="Email znajomego" class="inp-left" />
				<button id="invite-submit" class="btn-right">Wyślij</button>
			</div>
		</div>
	</div>
</div>