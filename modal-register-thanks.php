<div id="modal-register-thanks" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Dzięki!</h1>
		<div class="info">
			Twoje konto zostało założone.
		</div>
		<button class="button-red" data-modal="modal-invite">Powiadom znajomych</button>
	</div>
</div>