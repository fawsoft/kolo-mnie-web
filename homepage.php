<?php
$searchDone = false;
if(isset($_POST['searchDone']) && $_POST['searchDone']=='done'){
	$searchDone = true;
}
?>
<?php
if(!$searchDone){
?>
<section class="intro">
	<div class="title">Jesteś u siebie</div>
	<div class="description">Znajdź nowe miejsca na mapie swojego miasta</div>
	<div class="inmputSearch mobile-show">
		<input id="homepage-searchbox-mob" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Wpisz, czego szukasz" />
		<button type="submit">&nbsp;</button>
	</div>
	<div class="fields mobile-none">
		<button class="btn-localize">&nbsp;</button>
		<input id="homepage-location" class="location" type="text" placeholder="Warszawa" >
		<input id="homepage-searchbox" class="name" type="text" placeholder="Wpisz nazwę lub tag" >
		<button id="homepage-searchbox-submmit" class="btn-search">Wyszukaj</button>
	</div>
	<div class="map mobile-none">
		<a href="map.php">
			<div class="icon"></div>
			<button class="btn-map">Pokaż na mapie</button>
		</a>
	</div>
</section>
<?php } ?>

<?php include("filter-bar.php"); ?>
<?php include("filter.php"); ?>

<section class="container">
	<div id="promos-container" class="row"></div>
	<div id="loadmoreajaxloader" style="display:none;"><center><img src="images/loader-infinite.gif" /></center></div>
</section>

<script>
$(document).ready(function() {
	<?php
	if(!$searchDone){
	?>
	vars.smallHeader = false;
	<?php } ?>
	reloadPlaces();
});
$(window).scroll(function(){
    infiniteScroll();
});
</script>