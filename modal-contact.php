<div id="modal-contact" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Kontakt</h1>
		<div>Masz pytania? Napisz do nas!</div>
		<textarea id="contact-content" placeholder="Treść wiadomości"></textarea>
		<input type="text" id="contact-email" placeholder="Twój E-mail" />
		<button id="contact-submit" class="button-red">Wyślij</button>
		<div class="info">
			<b>Kontakt z biurem</b>
			<br />
			Kolomnie Sp. z o.o.
			<br />
			ul. Marszałkowska 49 m. 12
			<br />
			00-230 Warszawa
			<br />
			NIP: 596-5-5435-5,
			REGON: 43424342
			<br />
			KRS: 04400404
			<div class="row people">
				<div class="col-lg-6 center">
					<b>Janek</b>
					<br />
					+48 660 586 289
				</div>
				<div class="col-lg-6 center">
					<b>Łukasz</b>
					<br />
					+48 660 586 280
				</div>
			</div>
		</div>
	</div>
</div>