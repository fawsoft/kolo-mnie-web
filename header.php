<?php include("begin.php"); ?>	
<?php include("modals.php"); ?>	
<?php 
	
	$loggedIn = false;
	if(isset($_GET['loggedin']) && $_GET['loggedin']==1){
		$loggedIn = true;
	}

	$searchPhrase = '';
	if(isset($_POST['headerSearchboxPhrase'])){
		$searchPhrase = $_POST['headerSearchboxPhrase'];
	}
?>
<script>
	$(document).ready(function() {
		vars.infiniteScroll.query = '<?php echo $searchPhrase; ?>';
	});
</script>
<?php include("menu-left.php"); ?>	
 
<header class="topBar">
 <div class="row"> 
 	<?php if(!$loggedIn){ ?>
 				<div class="mobile-show"><span class="profileAvatar"><img src="images/avatar-test.png"> <a href="login-mob.php">Zaloguj się</a></span></div>
	<?php }else{ ?>
		<div class="mobile-show"><span class="profileAvatar"><img src="images/fake-face2.png"> <a href="#">Adam Wróblewski</a></span></div>
	<?php } ?>
	<div class="menulabel mobile-none"> MENU </div>		 
	<div class="cd-menu-trigger btn-menu-mobile">
		<div class="menu_btn">
			
		<div class="myprofile-mob">
			<!--Log-in-->
			
		</div>
			<i><span class="cd-menu-icon"></span><div class="buble-mob">84</div></i>

		</div>
	</div>
		<div class="topin">
			<div id="logo">
				<a href="/" ><img src="images/logo_big.png" class="logo"></a>
				<a href="/" ><img src="images/logo@2x.png" class="logo-mobile"></a>			
			</div>
			<div class="mobile-iconTop">
				<a href="" class="mobSearch"></a>
				<a href="/map-mob.php" class="mobLocalization"></a>			 
				<img src="images/close.png" class="mobSearchClose" />
				<input class="mobSearchField" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Wpisz, czego szukasz" />
			</div>
				<div id="topSearch" class="hide">
				<form action="/" id="topForm" class="mobile-none" method="post">
					<input type="hidden" name="searchDone" value="done" />
					<button class="btn-localize-top">&nbsp;</button>
					<input class="topLocation" type="text" placeholder="Warszawa" >
					<input id="header-searchbox" class="topName" type="text" placeholder="Wpisz nazwę lub tag" name="headerSearchboxPhrase" value="<?php echo $searchPhrase; ?>" >
					<div class="search"><button type="submit" class="submit lupa">search</button></div>
				</form>
			</div>	
			<div class="right-top">	 
				<div class="topMap hide mobile-none"><a class="icoMap " href="map.php"></a></div>
				<a class="addOffer hide mobile-none" href="#" data-link="http://facebook.com" data-new-window="true">Dodaj ofertę</a>	
				<?php if(!$loggedIn){ ?>
					<button type="button" class="btn btn-rejestracja pull-right mobile-none" data-modal="modal-register">Zarejestruj się</button> 
					<button type="button" class="btn btn-logowanie pull-right mobile-none" data-modal="modal-login">Zaloguj się</button>  
				<?php }else{ ?>
					<div class="header-logged-in mobile-none">
						<div class="user-name">Antek Wróblewski</div>
						<div class="face" style="background:url(images/fake-face2.png) center center no-repeat; background-size:cover;"></div>
					</div>
					<div class="header-logged-menu">
						<div class="item" data-modal="modal-edit-profile">Edytuj profil</div>
						<div class="item" data-modal="modal-profile-settings">Ustawienia profilu</div>
						<div class="item" data-modal="modal-invite">Zaproś znajomych</div>
						<div class="item" data-modal="modal-logout">Wyloguj</div>
						<div class="social row">
							<div class="col-lg-3 text">
								Połącz konto z:
							</div>
							<div class="col-lg-9 icons">
								<a href="#"><div class="facebook"></div></a>
								<a href="#"><div class="foursquare"></div></a>
								<a href="#"><div class="instagram"></div></a>
								<a href="#"><div class="yelp"></div></a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>	
		</div>
		</div>
 
</header>
<main class="cd-main-content">
