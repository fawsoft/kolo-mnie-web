<?php include("header.php"); ?>

<section class="container no-results">
	<div class="title">
		<h1>Wyszukiwane:</h1>
		<span class="frase">relax (0)</span>
	</div>
	<div class="info">
		Podana przez Ciebie fraza nie została odnaleziona.
		<br />
		- Sprawdź, czy wszystkie słowa zostały poprawnie napisane.
		<br />
		- Spróbuj użyć innych słów kluczowych.
		<br />
		- Spróbuj użyć bardziej ogólnych słów kluczowych.
		<br /><br />
		Może chodziło Ci o: <span class="frase">relaks</span>
	</div>
	<div class="categories">
		<a href="category.php?c=sztuka-i-rozrywka">
			<div class="category">
				<div class="box sztuka-i-rozrywka">
					<div class="amount">55</div>
				</div>
				<div class="text">Sztuka i rozrywka</div>
			</div>
		</a>
		<a href="category.php?c=jedzenie">
			<div class="category">
				<div class="box jedzenie">
					<div class="amount">17</div>
				</div>
				<div class="text">Jedzenie</div>
			</div>
		</a>
		<a href="category.php?c=zycie-nocne">
			<div class="category">
				<div class="box zycie-nocne">
					<div class="amount">6</div>
				</div>
				<div class="text">Życie nocne</div>
			</div>
		</a>
		<a href="category.php?c=uslugi">
			<div class="category">
				<div class="box uslugi">
					<div class="amount">1</div>
				</div>
				<div class="text">Usługi</div>
			</div>
		</a>
	</div>
	<div class="clearfix"></div>
	<div class="divider"></div>
</section>

<?php include("filter-bar.php"); ?>

<?php include("filter.php"); ?>

<section class="container">
	<div class="row">
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
		<?php include("place-box.php"); ?>
	</div>
</section>
<?php include("footer.php"); ?>

