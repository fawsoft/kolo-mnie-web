<div id="modal-login" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Logowanie</h1>
		<a href="#"><img src="images/zaloguj-przez-facebooka.png" border="0" /></a>
		<div class="fb-info">
			Łącząc konta, poznasz miejsca, w których byli Twoi znajomi oraz ich rekomendacje.
		</div>
		<div class="or">
			lub
		</div>
		<form id="form-login" action="/?loggedin=1" method="post"> <!-- ?loggedin=1 do usunięcia, to jest tylko aby zasymulować logowanie -->
			<input type="email" id="login-email" name="loginEmail" placeholder="E-mail" required />
			<input id="login-password" name="loginPassword" placeholder="Hasło" type="password" required />
			<button class="button-red">Zaloguj się</button>
			<div class="options">
				<div class="lost-password" data-modal="modal-change-password-request">Nie pamiętasz hasła?</div>
				<input type="checkbox" id="login-remember-me" name="loginRememberMe" /> <label for="login-remember-me">Zapamiętaj mnie</label>
			</div>
		</form>
		<div class="register-text">
			Nie masz konta na Kolomnie.pl?
		</div>
		<button class="button-red" data-modal="modal-register">Zarejestruj się</button>
	</div>
</div>