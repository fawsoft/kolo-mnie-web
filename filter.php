<section id="openFilter-box">
		<div class="container pagination-centered">
			<div class="row">
				<div class="col-md-4"><p>Średnia cena dania głównego:</p> </div> 
				<div class="col-md-8"> <input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="200" data-slider-step="5" data-slider-value="[10,200]"/> </div> 
			</div>
			<div class="row">
				<div class="col-md-3"><p>Płatność Kartą:</p></div> 
				<div class="col-md-3"> 
					<input type="radio" id="radioPlatnosc2" name="platnosc" value="false">
					<label for="radioPlatnosc2" class="radious-l">nie</label>
					<input type="radio" id="radioPlatnosc1" name="platnosc" value="true" checked >
					<label for="radioPlatnosc1" class="radious-r">tak</label>					
				</div>
				<div class="col-md-3"><p>Dostawa na telefon:</p></div> 
				<div class="col-md-3"> 
					<input type="radio" id="radioDostawa2" name="dostawa" value="false" >
					<label for="radioDostawa2" class="radious-l">nie</label>
					<input type="radio" id="radioDostawa1" name="dostawa" value="true" checked >
					<label for="radioDostawa1" class="radious-r">tak</label>					
				</div>
			</div>
			<div class="row">
				<div class="col-md-3"><p>Przyjazne dzieciom:</p></div> 
				<div class="col-md-3"> 
					<input type="radio" id="radioDzieci2" name="dzieci" value="false" checked>
					<label for="radioDzieci2" class="radious-l">nie</label>
					<input type="radio" id="radioDzieci1" name="dzieci" value="true">
					<label for="radioDzieci1" class="radious-r">tak</label>					
				</div>
				<div class="col-md-3"><p>Przyjazne zwierzętom:</p></div> 
				<div class="col-md-3" > 					
					<input type="radio" id="radioZwierzeta2" name="zwierzeta" value="false" checked>
					<label for="radioZwierzeta2" class="radious-l">nie</label>
					<input type="radio" id="radioZwierzeta1" name="zwierzeta" value="true">
					<label for="radioZwierzeta1" class="radious-r">tak</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4 centered">
					<a href="#" class="clear-filtr">Wyczyść Filtry</a>
					<button id="filter-places-submit" type="button" class="btn btn-blue-filtr">Zapisz</button></div> 
			</div>
		</div>
</section>