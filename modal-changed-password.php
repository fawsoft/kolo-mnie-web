<div id="modal-changed-password" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Hasło zmienione</h1>
		<div>Na podany email wysłaliśmy dalsze informacje prowadzące do zmiany hasła.</div> 
		<button class="button-red modal-close">Kontynuuj</button>
	</div>
</div>