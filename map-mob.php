<?php include("begin.php"); ?>
<?php include("modals.php"); ?> 

<div id="lg-mobile">
	<div class="container-fluid">
		<div class="row">
			<div class="header-mobile">
				<a href="javascript:window.history.back();"><img src="images/assets/reg_back.png" height="50" border="0" /></a>
				<input class="map-mob-search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" placeholder="Wpisz, czego szukasz" />
			</div>
		</div>
		<div class="row">
			<div class="map-mob-order">
				<select id="map-sortby">
					<option value="distance">Najbliżej</option>
					<option value="popularity">Najpopularniejsze</option>
					<option value="rating">Najwyżej oceniane</option>
				</select>
			</div>
			<div id="map-big"></div>
		</div>
	</div>
</div>

<style>
	body{
		overflow:visible;
	}
</style>

<!--js-->
<script type="text/javascript">
	var mapVars = {
		sortBy: 'distance',
		markers: [],
		phrase:'',
		userPosition:null
	};
	
	jQuery(document).ready(function($){
		$('#map-big').height($(window).height() - $('.header-mobile').height());
		initialize();
		
		$( ".map-mob-search" ).autocomplete({
			minLength: 0,
			source: 'autocomplete.php',
			focus: function( event, ui ) {
				$( ".map-mob-search" ).val( ui.item.label ); 
				mapVars.phrase = ui.item.label;
				return false;
			},
			select: function( event, ui ) {
				$( ".map-mob-search" ).val( ui.item.label ); 
				mapVars.phrase = ui.item.label;
				getPoints();
				return false;
			}
		})
		.autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append( item.label )
				.appendTo( ul );
		};
	
		$('#map-sortby').on('change', function(){
			mapVars.sortBy = $(this).val();
			getPoints();
		});
	});

	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();  
	var map = null;
	
	function getPoints(){
		$.ajax({
			method: 'POST',
			url: 'loadpoints.php',
			dataType: 'json',
			data:{
				sortBy: mapVars.sortBy,
				phrase: mapVars.phrase
			},
			success:function(points){
				bounds = new google.maps.LatLngBounds();
				if(mapVars.userPosition){
					bounds.extend(mapVars.userPosition);
				}
				for(var i=0; i<mapVars.markers.length; i++){
					mapVars.markers[i].setMap(null);
				}
				for(var i=0; i<points.length; i++){
					myLatlng = new google.maps.LatLng(points[i].lat,points[i].lon);
					marker = new google.maps.Marker({
						position: myLatlng,
						map: map,
						title: points[i].name,
						icon: 'images/lokalizacja_ikona.png'
					});
					mapVars.markers.push(marker);
					bounds.extend(marker.position);
					google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
						return function() {
							infowindow.setContent('<table><tr><td valign="top"><div style="background:url('+points[i].img+') center center no-repeat;background-size:cover;width:100px;height:100px;" /></td><td width="10"></td><td valign="top"><div style="font-weight:600;font-size:15px;">'+points[i].name+'</div><div style="color:#8B8B8B;">'+points[i].address+'</div><div class="stars stars-'+points[i].stars+'"></div></td></table>');
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				map.fitBounds(bounds);
			},
			error:function(jqXHR, textStatus, errorThrown){
				alert(textStatus+' '+errorThrown);
			}
		});
	}
	function initialize() {
		var mapOptions = {
			center: { lat: -34.397, lng: 150.644},
			zoom: 8,
			disableDefaultUI: true
		};
		map = new google.maps.Map(document.getElementById('map-big'), mapOptions);
		getPoints();
		getLocation();
	}
	function addUserMarker(position){ 
		var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: 'Twoje położenie',
			icon: 'images/user_position_icon.png'
		});
		mapVars.userPosition = marker.position;
		bounds.extend(marker.position);
		map.fitBounds(bounds);
	}
	function getLocation() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(addUserMarker);
		}
	} 
</script>

<?php include("end.php"); ?>

