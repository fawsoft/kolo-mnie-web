<?php include("header.php"); ?>

<section class="container breadcrumb">
	<ul>
		<li><a href="/">Strona główna</a></li>
		<li>Kategorie</li>
	</ul>
</section>

<div class="container">
<h2>Kategorie</h2>
	<div class="categories">
		<a href="category.php?c=sztuka-i-rozrywka">
			<div class="category">
				<div class="box sztuka-i-rozrywka"></div>
				<div class="text">Sztuka i rozrywka</div>
			</div>
		</a>
		<a href="category.php?c=jedzenie">
			<div class="category">
				<div class="box jedzenie"></div>
				<div class="text">Jedzenie</div>
			</div>
		</a>
		<a href="category.php?c=zycie-nocne">
			<div class="category">
				<div class="box zycie-nocne"></div>
				<div class="text">Życie nocne</div>
			</div>
		</a>
		<a href="category.php?c=uslugi">
			<div class="category">
				<div class="box uslugi"></div>
				<div class="text">Usługi</div>
			</div>
		</a>
		<a href="category.php?c=edukacja">
			<div class="category">
				<div class="box edukacja"></div>
				<div class="text">Edukacja</div>
			</div>
		</a>
		<a href="category.php?c=religia">
			<div class="category">
				<div class="box religia"></div>
				<div class="text">Religia</div>
			</div>
		</a>
		<a href="category.php?c=zdrowie-i-uroda">
			<div class="category">
				<div class="box zdrowie-i-uroda"></div>
				<div class="text">Zdrowie i uroda</div>
			</div>
		</a>
		<a href="category.php?c=zakupy">
			<div class="category">
				<div class="box zakupy"></div>
				<div class="text">Zakupy</div>
			</div>
		</a>
		<a href="category.php?c=motoryzacja">
			<div class="category">
				<div class="box motoryzacja"></div>
				<div class="text">Motoryzacja</div>
			</div>
		</a>
	</div>
</div>
<div style="height:700px;"></div>
<?php include("footer.php"); ?>

