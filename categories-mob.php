<?php include("begin.php"); ?>
<?php include("modals.php"); ?> 
<?php 
	$categories = [
		[
			"key" => "sztuka-i-rozrywka",
			"name" => "Sztuka i rozrywka",
			"subcategories" => [
				["key" => "automaty", "value" => "Automaty"],
				["key" => "kasyna", "value" => "Kasyna"],
				["key" => "kluby-komediowe", "value" => "Kluby komediowe"],
				["key" => "kultura-i-rozrywka", "value" => "Kultura i rozrywka"],
				["key" => "kina", "value" => "Kina"],
				["key" => "teatry", "value" => "Teatry"],
				["key" => "kluby-bilardowe", "value" => "Kluby bilardowe"],
				["key" => "obiekty-sportowe", "value" => "Obiekty sportowe"],
				["key" => "teatry-niezalezna", "value" => "Teatry niezależne"],
				["key" => "na-dworze", "value" => "Na dworze"],
				["key" => "muzyka", "value" => "Muzyka"]
			]
		],
		[
			"key" => "jedzenie",
			"name" => "Jedzenie",
			"subcategories" => [
				["key" => "inne-zwiazane-z-jedzeniem", "value" => "Inne związane z jedzeniem"],
				["key" => "grill", "value" => "Grill"],
				["key" => "sklepy-z-bajglami", "value" => "Sklepy z bajglami"],
				["key" => "piekarnie", "value" => "Piekarnie"],
				["key" => "bistra", "value" => "Bistra"],
				["key" => "sniadania", "value" => "Śniadania"],
				["key" => "herbaty-z-babelkami", "value" => "Herbaty z bąbelkami"],
				["key" => "bufety", "value" => "Bufety"],
				["key" => "burgery", "value" => "Burgery"],
				["key" => "buritto", "value" => "Buritto"],
				["key" => "kawiarnie", "value" => "Kawiarnie"]
			]
		],
		[
			"key" => "zycie-nocne",
			"name" => "Życie nocne",
			"subcategories" => [
				["key" => "bary", "value" => "Bary"],
				["key" => "lounge", "value" => "Lounge"],
				["key" => "kluby", "value" => "Kluby"],
				["key" => "inne-zycie-nocne", "value" => "Inne życie nocne"],
				["key" => "puby", "value" => "Puby"],
				["key" => "kluby-ze-striptizem", "value" => "Kluby ze striptizem"],
				["key" => "bary-plazowe", "value" => "Bary plażowe"],
				["key" => "ogrodki-piwne", "value" => "Ogródki piwne"],
				["key" => "piwiarnie", "value" => "Piwniarnie"],
				["key" => "bary-szampanskie", "value" => "Bary szampańskie"],
				["key" => "bary-koktajlowe", "value" => "Bary koktajlowe"]
			]
		],
		[
			"key" => "uslugi",
			"name" => "Usługi",
			"subcategories" => [
				["key" => "centra-duchowe", "value" => "Centra duchowe"],
				["key" => "finanse-i-bankowosc", "value" => "Finanse i bankowość"],
				["key" => "uslugi-prawne", "value" => "Usługi prawne"],
				["key" => "bankomaty", "value" => "Bankomaty"],
				["key" => "banki", "value" => "Banki"],
				["key" => "pomoc-finansowa", "value" => "Pomoc finansowa"],
				["key" => "planowanie-finansowe", "value" => "Planowanie finansowe"],
				["key" => "uslugi-finansowe", "value" => "Usługi finansowe"],
				["key" => "prawnicy-bankructwa", "value" => "Prawnicy bankructwa"],
				["key" => "prawnicy-wslasnosci-intelektualnej", "value" => "Prawnicy własności intelektualnej"],
				["key" => "prawnicy-prawa-korporacyjnego", "value" => "Prawnicy prawa korporacyjnego"]
			]
		],
		[
			"key" => "edukacja",
			"name" => "Edukacja",
			"subcategories" => [
				["key" => "biblioteki", "value" => "Biblioteki"],
				["key" => "szkolnictwo", "value" => "Szkolnictwo"],
				["key" => "pozostale-szkoly", "value" => "Pozostałe szkoły"],
				["key" => "inne-uslugi-edukacyjne", "value" => "Inne usługi edukacyjne"],
				["key" => "szkoly-publiczne", "value" => "Szkoły publiczne"],
				["key" => "szkoly-podstawowe", "value" => "Szkoły podstawowe"],
				["key" => "szkoly-srednie", "value" => "Szkoły średnie"],
				["key" => "szkoly-gimnazjalne", "value" => "Szkoły gimnazjalne"],
				["key" => "przedszkola", "value" => "Przedszkola"],
				["key" => "szkoly-prywatne", "value" => "Szkoły prywatne"],
				["key" => "szkoly-religijne", "value" => "Szkoły religijne"]
			]
		],
		[
			"key" => "religia",
			"name" => "Religia",
			"subcategories" => [
				["key" => "koscioly", "value" => "Kościoły"],
				["key" => "zwiazane-z-religia", "value" => "Związane z religią"],
				["key" => "cmentarze", "value" => "Cmentarze"],
				["key" => "swiatynie-buddystyczne", "value" => "Świątynie buddystyczne"],
				["key" => "swiatynie-hinduistyczne", "value" => "Świątynie hinduistyczne"],
				["key" => "klasztory", "value" => "Klasztory"],
				["key" => "meczety", "value" => "Meczety"],
				["key" => "pokoje-modlitewne", "value" => "Pokoje modlitewne"],
				["key" => "kaplice", "value" => "Kaplice"],
				["key" => "synagogi", "value" => "Synagogi"],
				["key" => "swiatynie", "value" => "Świątynie"]
			]
		],
		[
			"key" => "zdrowie-i-uroda",
			"name" => "Zdrowie i uroda",
			"subcategories" => [
				["key" => "lekarze", "value" => "Lekarze"],
				["key" => "szpitale-i-kliniki", "value" => "Szpitale i kliniki"],
				["key" => "dentystyka", "value" => "Dentystyka"],
				["key" => "apteki", "value" => "Apteki"],
				["key" => "inne-uslugi-medyczne", "value" => "Inne usługi medyczne"],
				["key" => "uroda", "value" => "Uroda"],
				["key" => "tatuaze-i-piercing", "value" => "Tatuaże i piercing"],
				["key" => "okulisci", "value" => "Okuliści"],
				["key" => "alergolog", "value" => "Alergolog"],
				["key" => "lekarz-rodzinny", "value" => "Lekarz Rodzinny"],
				["key" => "ginekolodzy", "value" => "Ginekolodzy"]
			]
		],
		[
			"key" => "zakupy",
			"name" => "Zakupy",
			"subcategories" => [
				["key" => "sklepy-odziezowe", "value" => "Sklepy odzieżowe"],
				["key" => "sklepy-spozywcze", "value" => "Sklepy spożywcze"],
				["key" => "prasa-ksiazki-filmy-muzyka-gry", "value" => "Prasa, książki, filmy, muzyka, gry"],
				["key" => "do-domow", "value" => "Do domów"],
				["key" => "inne", "value" => "Inne"],
				["key" => "sklepy-odziezowe-dla-niemowlat", "value" => "Sklepy odzieżowe dla niemowląt"],
				["key" => "sklepy-z-akcesoriami", "value" => "Sklepy z akcesoriami"],
				["key" => "butiki", "value" => "Butiki"],
				["key" => "sklepy-odziezowe-dla-dzieci", "value" => "Sklepy odzieżowe dla dzieci"],
				["key" => "sklepy-z-bielizna", "value" => "Sklepy z bielizną"],
				["key" => "sklepy-odziezowe-dla-mezczyzn", "value" => "Sklepy odzieżowe dla mężczyzn"]
			]
		],
		[
			"key" => "motoryzacja",
			"name" => "Motoryzacja",
			"subcategories" => [
				["key" => "naprawa-pojazdow", "value" => "Naprawa pojazdów"],
				["key" => "wynajem-pojazdow", "value" => "Wynajem pojazdów"],
				["key" => "czesci-samochodowe-i-akcesoria", "value" => "Części samochodowe i akcesoria"],
				["key" => "salony-samochodowe", "value" => "Salony samochodowe"],
				["key" => "myjnie-samochodowe", "value" => "Myjnie samochodowe"],
				["key" => "salony-motocykli", "value" => "Salony motocykli"],
				["key" => "naprawa-motocykli", "value" => "Naprawa motocykli"],
				["key" => "warsztaty-samochodowe", "value" => "Warsztaty samochodowe"],
				["key" => "naprawa-szyb", "value" => "Naprawa szyb"],
				["key" => "tuning-samochodow", "value" => "Tuning samochodów"],
				["key" => "renowacje-samochodow", "value" => "Renowacje samochodów"]
			]
		]
	];
?>
<div id="lg-mobile">
	<div class="container-fluid">
		<div class="row">
			<div class="header-mobile"> 
				<div class="col-lg-2 col-md-2 col-xs-2 "><a href="javascript:window.history.back();"><img src="images/assets/reg_back.png" height="50" border="0" /></a></div>
                <div class="col-lg-8 col-md-8 col-xs-8 center header-mobile-title">Wybierz kategorię</div>
			</div>
		</div>
		<div class="row">
			<ul class="categories-mob">
				<?php
					for($i=0; $i<count($categories); $i++){
						echo '<li data-category="'.$categories[$i]['key'].'">';
						echo '<div class="catlogo"><img src="images/'.$categories[$i]['key'].'-black.png" /></div>';
						echo $categories[$i]['name'];
						echo '</li>';
						echo '<ul id="subcategory-'.$categories[$i]['key'].'" class="subcategories-mob">';
						for($j=0; $j<count($categories[$i]['subcategories']); $j++){
							echo '<li>';
							echo '<input id="subcategory-'.$categories[$i]['subcategories'][$j]['value'].'" type="checkbox" /> ';
							echo '<label for="subcategory-'.$categories[$i]['subcategories'][$j]['value'].'">'.$categories[$i]['subcategories'][$j]['value'].'</label>';
							echo '</li>';
						}
						echo '</ul>';
					}
				?>
			</ul>
		</div>
	</div>
</div>
 

<?php include("end.php"); ?>

