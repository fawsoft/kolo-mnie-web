<div id="modal-profile-settings" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Ustawienia profilu</h1>
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td align="right">
					Otrzymywanie rekomendacji:
				</td>
				<td width="10"></td>
				<td>
					<input id="radioProfileSettings11" type="radio" value="false" name="profile-settings-1">
					<label class="radious-l" for="radioProfileSettings11">nie</label>
					<input id="radioProfileSettings12" type="radio" checked="" value="true" name="profile-settings-1">
					<label class="radious-r" for="radioProfileSettings12">tak</label>
				</td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td align="right">
					Personalizuj wyniki:
				</td>
				<td width="10"></td>
				<td>
					<input id="radioProfileSettings21" type="radio" value="false" name="profile-settings-2">
					<label class="radious-l" for="radioProfileSettings21">nie</label>
					<input id="radioProfileSettings22" type="radio" checked="" value="true" name="profile-settings-2">
					<label class="radious-r" for="radioProfileSettings22">tak</label>
				</td>
			</tr>
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr>
				<td align="right">
					Poinformuj o promocjach:
				</td>
				<td width="10"></td>
				<td>
					<input id="radioProfileSettings31" type="radio" value="false" name="profile-settings-3">
					<label class="radious-l" for="radioProfileSettings31">nie</label>
					<input id="radioProfileSettings32" type="radio" checked="" value="true" name="profile-settings-3">
					<label class="radious-r" for="radioProfileSettings32">tak</label>
				</td>
			</tr> 
			<tr>
				<td align="right" valign="middle">
					<a id="profile-settings-reset" href="#" class="link">Wróć do ustawień początkowych</a>
				</td>
				<td width="10"></td>
				<td valign="middle">
					<button id="profile-settings-submit" class="button-red">Zapisz</button>
				</td>
			</tr>
		</table>
	</div>
</div>