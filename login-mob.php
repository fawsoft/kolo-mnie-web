<?php include("begin.php"); ?>
    <div id="lg-mobile">
        <div class="container-fluid">
            <div class="row">
                <div class="header-mob">
                    <div class="col-lg-8 col-md-8 col-xs-8">Logowanie</div>
                    <div class="col-lg-4 col-md-4 col-xs-4"><a href="javascript:window.history.back();">pomiń</a> </div>
                    
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4">
                    <form action="/" id="mobile-log" method="post">
                        <p>Zaloguj się do Kolomnie.pl</p>
                        <div>
                            <input id="login-mob-email" type="email" class="form-control"  placeholder="E-mail">
                        </div>
                        <div>
                            <input id="login-mob-password" type="password" class="form-control"  placeholder="Hasło">
                        </div>
                        <div>
                            <button id="login-mob-submit" type="button" class="btn btn-red">zaloguj się</button>
                        </div>      
                        <div>
                            <p>Nie masz konta na Kolomnie.pl<p>
                            <button type="button" class="btn btn-red" onclick="javascript:document.location.href='/register-mob.php'">zarejestruj się</button>  
                        </div>
                        <div>
                            <p>lub</p>
                            <button type="button" class="btn btn-blue">zaloguj się przez facebook</button>
                            <p>Łącząc konta poznasz miejsca, w których byli Twoi znajomi oraz poznasz ich rekomendacje.</p>
                        </div>      
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include("end.php"); ?>
