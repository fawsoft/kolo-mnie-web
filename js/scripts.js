var vars = {
	openedModalId: '',
	topPositionFormodal: 0,
	geocoder: null,
	FacebookAppId: 548254401977870,
	smallHeader:true,
	filter:{
		priceMin:10,
		priceMax:200,
		platnosc:1,
		dostawa:1,
		dzieci:0,
		zwierzeta:0
	},
	infiniteScroll:{
		page:0,
		isContent:true,
		block:false,
		sortBy: 'distance',
		priceMin:10,
		priceMax:200,
		platnosc:1,
		dostawa:1,
		dzieci:0,
		zwierzeta:0,
		type:'places',
		when:'today',
		category:'',
		subcategories:[],
		isCategoryPage: false,
		location:{
			address:'Warszawa',
			lat:0,
			lon:0
		},
		query:''
	}
}
/*--Scroll Fix Header--*/

$(window).scroll(function(){
	if(vars.smallHeader){
		return;
	}
	if($(this).scrollTop() > 75) {
		shrinkHeader();
		return false;
	}
	else if($(this).scrollTop() < 75) {
		$('.topBar').removeClass('topBarfix');
		$('.main-page').removeClass('main-pagefix');
		$('.hide').removeClass('show');
		$('.btn-logowanie').removeClass('btn-fix');
		$('.btn-rejestracja').removeClass('btn-fix');
		$('.logo').removeClass('logo-fix');
		$('.menu_btn').removeClass('menu_btn-fix');   
		$('.header-logged-in').removeClass('header-logged-in-fix');
		$('.header-logged-menu').removeClass('header-logged-menu-fixed');
		$('.menulabel').removeClass('menulabel-fixed');
		
	}
});
 
function shrinkHeader(){
	$('.topBar').addClass('topBarfix');
	$('.main-page').addClass('main-pagefix');  
	$('.hide').addClass('show');
	$('.btn-logowanie').addClass('btn-fix'); 
	$('.btn-rejestracja').addClass('btn-fix');
	$('.logo').addClass('logo-fix'); 
	$('.menu_btn').addClass('menu_btn-fix');  
	$('.header-logged-in').addClass('header-logged-in-fix');
	$('.header-logged-menu').addClass('header-logged-menu-fixed');
	$('.menulabel').addClass('menulabel-fixed');
}
 

/*-- Scrollable mobile menu--*/
 
 jQuery(document).ready(function($){
 
	if(vars.smallHeader){
		shrinkHeader();
	}
 
	var $lateral_menu_trigger = $('.cd-menu-trigger'),
		$content_wrapper = $('.cd-main-content'),
		$navigation = $('header');

	//open-close lateral menu clicking on the menu icon
	$lateral_menu_trigger.on('click', function(event){
		event.preventDefault();
		
		$lateral_menu_trigger.toggleClass('is-clicked'); 
		$content_wrapper.toggleClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
			$('body').toggleClass('overflow-hidden');
		});
		$('#cd-lateral-nav').toggleClass('lateral-menu-is-open');
		$('#ulMenu').toggleClass('lateral-menu-is-open');
		$('.profileAvatar').toggleClass('lateral-menu-is-open');
		$('.menulabel').toggleClass('lateral-menu-is-open');  
		//check if transitions are not supported - i.e. in IE9
		if($('html').hasClass('no-csstransitions')) {
			$('body').toggleClass('overflow-hidden');
			$(".sub-menu").css({'display': 'none'}); 
		}
	});

	//close lateral menu clicking outside the menu itself
	$content_wrapper.on('click', function(event){
		if( !$(event.target).is('.cd-menu-trigger, .cd-menu-trigger span') ) {
			$lateral_menu_trigger.removeClass('is-clicked');
			$navigation.removeClass('lateral-menu-is-open');
			$(".sub-menu").css({'display': 'none'}); 
			$content_wrapper.removeClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').removeClass('overflow-hidden');
			});
			$('#cd-lateral-nav').removeClass('lateral-menu-is-open');
			$('#ulMenu').removeClass('lateral-menu-is-open');
			$('.profileAvatar').removeClass('lateral-menu-is-open'); 
			$('.menulabel').removeClass('lateral-menu-is-open'); 
			$(".sub-menu").css({'display': 'none'});  
			//check if transitions are not supported
			if($('html').hasClass('no-csstransitions')) {
			$('body').removeClass('overflow-hidden');
			}
		}
	});

	//open (or close) submenu items in the lateral menu. Close all the other open submenu items.
	$('.item-has-children').children('a').on('click', function(event){ 
		$(this).toggleClass('submenu-open')
		.next('.sub-menu').slideToggle(300).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(300);
	});
});



  
/*--Show/Hide filtres--*/

$(document).ready(function() { 
	
	$('#filter-bar-events-calendar').datepicker({
		monthNames: [ "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień" ],
		dayNamesMin: [ "Nd", "Po", "Wt", "Śr", "Cz", "Pt", "So" ],
		dateFormat: 'dd/mm/yy',
		minDate:0,
		onSelect: function(date) {
            $('#filter-bar-events-date').html(date);
			$('#filter-bar-events-calendar').hide();
			vars.infiniteScroll.when = date;
			reloadPlaces();
        }
	});

    $('.filter').click(function() {
        $('#openFilter-box').slideToggle('1100');
        var src = $("#bg3").attr('src') == "images/assets/arrow_filter_down.png" ? "images/assets/arrow_filter_up.png" : "images/assets/arrow_filter_down.png";
        $("#bg3").attr('src', src);
    });
	
	$('[data-modal]').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		vars.topPositionFormodal = $(window).scrollTop();
		$(window).scrollTop(0);
		if(vars.openedModalId){
			$('#'+vars.openedModalId).fadeOut();
			vars.openedModalId = '';
		}
		showApla();
		vars.openedModalId = $(this).data('modal');
		var windowHeight = $(window).height();
		var modalHeight = $('#'+vars.openedModalId).height();
		if(modalHeight > windowHeight){
			$('#'+vars.openedModalId).css({top:'10px'});
		}
		$('#'+vars.openedModalId).fadeIn();
	});
	
	$('[data-link]').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		if($(this).data('new-window')==true){
			window.open($(this).data('link'), "_blank");
		}else{
			document.location.href = $(this).data('link');
		}
	});
	
	$('#apla').click(function(){
		hideModals();
	});
	
	$('.modal .close, .modal .modal-close').click(function(){
		hideModals();
	});
	
	/*$('#register-submit').click(function(){
		showModal('modal-register-thanks');
	});*/
	
	$('.btn-localize').click(function(){
		if (navigator.geolocation) {
			$(this).css({background: '#EF4036 url(images/loader-geo.gif) center center no-repeat'});
			navigator.geolocation.getCurrentPosition(pastePosition);
		}else{
			alert('Twoje urządzenie nie posiada możliwości geolokalizacji.');
		}
	});
	
	$('.filter-bar-option').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		$('.filter-bar-option').removeClass('selected');
		$(this).addClass('selected');
		vars.infiniteScroll.sortBy = $(this).data('sortby');
		reloadPlaces();
	});
	
	$('.filter-bar-events-option').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		$('.filter-bar-events-option').removeClass('selected');
		$(this).addClass('selected');
		if($(this).data('when') == 'calendar'){
			$('#filter-bar-events-calendar').toggle();
		}else{
			vars.infiniteScroll.when = $(this).data('when');
			reloadPlaces();
		}
	});
	
	$('#filter-bar-card-pay').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		$(this).remove();
		$('#radioPlatnosc2').prop('checked', true);
		reloadPlaces();
	});
	
	$('.clear-filtr').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		$('#radioPlatnosc1').prop('checked', true);
		$('#radioDostawa1').prop('checked', true);
		$('#radioDzieci2').prop('checked', true);
		$('#radioZwierzeta2').prop('checked', true); 
		$('#ex2').slider('setValue', [10,200]);
		reloadPlaces();
	});
	 
    $( "#homepage-searchbox" ).autocomplete({
		minLength: 0,
		source: 'autocomplete.php',
		focus: function( event, ui ) {
			$( "#homepage-searchbox" ).val( ui.item.label );
			$( "#header-searchbox" ).val( ui.item.label );
			return false;
		},
		select: function( event, ui ) {
			$( "#homepage-searchbox" ).val( ui.item.label ); 
			$( "#header-searchbox" ).val( ui.item.label );
			return false;
		}
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li>" )
			.append( "<table style='margin:0;padding:0;' cellspacing='0' cellpadding='0'><tr><td valign='top'><div style='background:url(images/"+item.category+"-black.png) center center no-repeat;width:40px;height:40px;'></div></td><td width='10'></td><td valign='top'><div><b>" + item.label + "</b></div><div style='color:#A4A4A4;font-size:14px;'>"  + item.address + "</div></td></tr></table>" )
			.appendTo( ul );
    };
	
	$( "#header-searchbox" ).autocomplete({
		minLength: 0,
		source: 'autocomplete.php',
		focus: function( event, ui ) {
			$( "#header-searchbox" ).val( ui.item.label );
			$( "#homepage-searchbox" ).val( ui.item.label ); 
			return false;
		},
		select: function( event, ui ) {
			$( "#header-searchbox" ).val( ui.item.label ); 
			$( "#homepage-searchbox" ).val( ui.item.label ); 
			return false;
		}
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li>" )
			.append( "<table style='margin:0;padding:0;' cellspacing='0' cellpadding='0'><tr><td valign='top'><div style='background:url(images/"+item.category+"-black.png) center center no-repeat;width:40px;height:40px;'></div></td><td width='10'></td><td valign='top'><div><b>" + item.label + "</b></div><div style='color:#A4A4A4;font-size:14px;'>"  + item.address + "</div></td></tr></table>" )
			.appendTo( ul );
    };
	
	$( ".mobSearchField" ).autocomplete({
		minLength: 0,
		source: 'autocomplete.php',
		focus: function( event, ui ) {
			$( ".mobSearchField" ).val( ui.item.label ); 
			return false;
		},
		select: function( event, ui ) {
			$( ".mobSearchField" ).val( ui.item.label ); 
			return false;
		}
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li>" )
			.append(item.label)
			.appendTo( ul );
    };
	
	$( "#homepage-searchbox-mob" ).autocomplete({
		minLength: 0,
		source: 'autocomplete.php',
		focus: function( event, ui ) {
			$( "#homepage-searchbox-mob" ).val( ui.item.label ); 
			return false;
		},
		select: function( event, ui ) {
			$( "#homepage-searchbox-mob" ).val( ui.item.label ); 
			return false;
		}
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
		return $( "<li>" )
			.append(item.label)
			.appendTo( ul );
    };
	
	$('#form-login').on('submit', function(){
		var validTxt = '';
		if($('#login-email').val() == ''){
			validTxt += '\nPole email nie może być puste';
		}else if(!validateEmail($('#login-email').val())){
			validTxt += '\nPole email ma niepoprawny format';
		}
		if($('#login-password').val() == ''){
			validTxt += '\nPole hasło nie może być puste';
		}
		if(validTxt != ''){
			alert(validTxt);
			return false;
		}
		return true;
	});
	
	/*$('#form-register').on('submit', function(){
		return true;
	});*/
	
	$('#contact-submit').on('click', function(){
		var validTxt = '';
		if($('#contact-content').val() == ''){
			validTxt += '\nPole treść nie może być puste.';
		}
		if($('#contact-email').val() == ''){
			validTxt += '\nPole email nie może być puste.';
		}else if(!validateEmail($('#contact-email').val())){
			validTxt += '\nPole email ma niepoprawny format.';
		}
		if(validTxt != ''){
			alert(validTxt);
			return;
		}
		var params = {
			content: $('#contact-content').val(),
			email: $('#contact-email').val()
		} 
	});
	
	$('#invite-submit').on('click', function(){
		validTxt = '';
		if($('#invite-email').val() == ''){
			validTxt = '\nPole email nie może być puste.';
		}else if(!validateEmail($('#invite-email').val())){
			validTxt = '\nPole email ma niepoprawny format.';
		}
		if(validTxt != ''){
			alert(validTxt);
			return;
		}
		var params = {
			email: $('#invite-email').val()
		}
	});
	
	$('#profile-settings-reset').on('click', function(){
		$('#radioProfileSettings12').prop('checked', true);
		$('#radioProfileSettings22').prop('checked', true);
		$('#radioProfileSettings32').prop('checked', true);
	});
	
	$('#profile-settings-submit').on('click', function(){
		var params = {
			receivingReccomendations: $('#radioProfileSettings11').prop('checked')?0:1,
			personalizeResults: $('#radioProfileSettings21').prop('checked')?0:1,
			informAboutPromos: $('#radioProfileSettings31').prop('checked')?0:1
		}
	});
	
	$('#report-poi-submit').on('click', function(){
		var params = {};
		if($('#report-poi-foursquare').prop('checked')){
			params.missingOnFoursquare = true;
		}
		if($('#report-poi-fb').prop('checked')){
			params.missingOnFacebook = true;
		}
		if($('#report-poi-invalid').prop('checked')){
			params.invalidPlace = true;
		}
		if($('#report-poi-spam').prop('checked')){
			params.spam = true;
		}
	});
	
	/*$('#promos-container').jscroll({
		contentSelector: '#promos-container'
	});*/
	
	$('#filter-places-submit').on('click', function(){
		reloadPlaces();
	});
	
	$('body').on('click', '.place-add-favorite', function(e){
		e.stopPropagation();
		e.preventDefault();
		$(this).html('Ulubione');
		$(this).addClass('favorite');
		//todo: send ajax
	});
	
	$('.like-event').on('click', function(){
		$(this).find('.text').html('&nbsp;');
		$(this).find('.heart').addClass('favorite');
		//todo: send ajax
	});
	
	$('.place-header .best, .place-header .best-text').on('click', function(){
		$('#place-favorite-text').html('&nbsp;');
		$('#place-favorite-heart').addClass('favorite');
		//todo: send ajax
	});
	
	$('.event-action').on('click', function(){
		$('.event-action').removeClass('active');
		$(this).addClass('active');
		//todo: send ajax
	});
	
	$('.subcategory-option').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		$(this).toggleClass('active');
		reloadPlaces();
	});
	
	$('.place-tags a').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		$('#header-searchbox').val($(this).html());
		$('#topForm').submit();
	});
	
	$('#placeCommentSubmit').on('click', function(){
		var content = $('#placeCommentContent').val();
		if(content == ''){
			alert('Wpisz treść komentarza');
			return;
		}
		//todo: send ajax
	});
	
	$('#homepage-searchbox-submmit').on('click', function(){
		vars.infiniteScroll.query = $('#homepage-searchbox').val();
		reloadPlaces();
	});
	
	$('body').on('click', '.jak-dojade, .jak-dojade-mob', function(e){
		e.stopPropagation();
		e.preventDefault();
		var fc = '';
		if(vars.infiniteScroll.location.lon!=0 && vars.infiniteScroll.location.lat){
			fc = '&fc='+vars.infiniteScroll.location.lat+':'+vars.infiniteScroll.location.lon;
		}
		var link = 'http://krakow.jakdojade.pl/index.html?fn=Koło+Mnie'+fc+'&tc='+$(this).data('lat')+':'+$(this).data('lon')+'';
		window.open(link, '_blank');
	});
	
	$('.share-facebook').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		shareFB($(this).data('url'), $(this).data('desc'), $(this).data('img'));
	});
	
	$('.share-twitter').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		shareTwitter($(this).data('url'), $(this).data('desc'));
	});
	
	$('.share-google').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		shareGoogle($(this).data('url'));
	});
	
	$('.expand-map').on('click', function(){
		var saddr = '';
		if(vars.infiniteScroll.location.lon != 0 && vars.infiniteScroll.location.lat != 0){
			saddr = 'saddr='+vars.infiniteScroll.location.lat+','+vars.infiniteScroll.location.lon;
		}
		var str = 'http://maps.google.com/?'+saddr+'&daddr='+$(this).data('lat')+','+$(this).data('lon');
		window.open(str,'_blank');
	});
	
	$('.rate').on('mouseover', function(){
		if($('#stars-rate').hasClass('frozen')) return;
		var stars = $(this).data('rate');
		$('#stars-rate').css({background: 'url(images/stars-'+stars+'.png)'});
	});
	
	$('.rate').on('mouseout', function(){
		if($('#stars-rate').hasClass('frozen')) return;
		$('#stars-rate').css({background: 'url(images/stars-0.png)'});
	});
	
	$('.rate').on('click', function(){
		var stars = $(this).data('rate');
		$('#stars-rate').css({background: 'url(images/stars-'+stars+'.png)'});
		$('#stars-rate').addClass('frozen');
		//todo: send ajax
	});
	
	$('.categories-mob li').on('click', function(){
		$('#subcategory-'+$(this).data('category')).slideToggle();
	});
	
	$('#login-mob-submit').on('click', function(){
		var validTxt = '';
		if($('#login-mob-email').val() == ''){
			validTxt += '\nPole email nie może być puste.';
		}else if(!validateEmail($('#login-mob-email').val())){
			validTxt += '\nPole email ma niepoprawny format.';
		}
		if($('#login-mob-password').val() == ''){
			validTxt += '\nPole hasło nie może być puste';
		}
		if(validTxt != ''){
			alert(validTxt);
			return;
		}
		$('#mobile-log').submit();
	});
	
	$('#register-mob-submit').on('click', function(){
		var validTxt = '';
		if($('#register-mob-email').val() == ''){
			validTxt += '\nPole email nie może być puste.';
		}else if(!validateEmail($('#register-mob-email').val())){
			validTxt += '\nPole email ma niepoprawny format.';
		}
		if($('#register-mob-password').val() == ''){
			validTxt += '\nPole hasło nie może być puste';
		}
		if(validTxt != ''){
			alert(validTxt);
			return;
		}
		$('.register-mob-form').submit();
	});
	
	$('.mobSearch').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		$('.logo-mobile').hide();
		$('.mobSearch').hide();
		$('.mobLocalization').hide();
		$('.mobSearchField').show();
		$('.mobSearchClose').show();
	});
	
	$('.mobSearchClose').on('click', function(){
		$('.logo-mobile').show();
		$('.mobSearch').show();
		$('.mobLocalization').show();
		$('.mobSearchField').hide();
		$('.mobSearchClose').hide();
	});
	
	$('.header-logged-in .user-name').on('click', function(){
		$('.header-logged-menu').fadeToggle();
	});
	
	$('.btn-localize-top').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		if (navigator.geolocation) {
			$(this).css({background: '#FFF url(images/loader-red.gif) center center no-repeat'});
			navigator.geolocation.getCurrentPosition(pastePosition);
		}else{
			alert('Twoje urządzenie nie posiada możliwości geolokalizacji.');
		}
	});
	
	$('.filtry-mob').on('change', function(){
		vars.infiniteScroll.sortBy = $(this).val();
		reloadPlaces();
	});
	
});

function shareGoogle(url){
	//alert(str); return;
	var str = 'https://plus.google.com/share?url='+url;
	window.open(str,'Google+', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

function shareTwitter(url, desc){
	//alert(url); return;
	var str = 'http://twitter.com/share?text='+desc+'&url='+url+'';
	window.open(str, "Twitter", "width=780, height=300, top=100, left=100, location=no, menubar=no, scrollbars=no, status=no");
}

function shareFB(url, desc, img){
	//alert(url+'\n'+desc+'\n'+img); return;
	desc = encodeURIComponent(desc);
	url = encodeURIComponent(url); 
	//img = encodeURIComponent(img); 
	var str = "https://www.facebook.com/dialog/feed?app_id=" + vars.FacebookAppId + "&display=popup&caption=" + desc + "&link=" + url + "&redirect_uri=" + url + "&picture=" + img + "&name=Kołomnie&description=kolomnie.pl&show_error=true";
	window.open(str, "Facebook", "width=780, height=300, top=100, left=100, location=no, menubar=no, scrollbars=no, status=no");
}

function getDistance(lat1, lon1, lat2, lon2){
	return roundDistance(getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2))
}

function roundDistance(d){
	if(d<=1){
		return Math.round(d*1000)+' m';
	}else if(d<100){
		return (Math.round(d*10)/10+'').replace('.', ',')+ ' km';
	}else{
		return Math.round(d)+' km';
	}
}

function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function infiniteScroll(){
	if($(window).scrollTop() == $(document).height() - $(window).height()){
		if(!vars.infiniteScroll.isContent) return;
		if(vars.infiniteScroll.block) return;
		vars.infiniteScroll.page += 1;
        $('div#loadmoreajaxloader').show();
        loadPlacesPage();
		vars.infiniteScroll.block = true;
		setTimeout(function(){
			vars.infiniteScroll.block = false;
		}, 1000);
    }
}

function reloadPlaces(){
	$('#promos-container').empty();
	$('div#loadmoreajaxloader').html('<center><img src="images/loader-infinite.gif" /></center>');
	$('div#loadmoreajaxloader').show();
	vars.infiniteScroll.block = false;
	vars.infiniteScroll.isContent = true;
	vars.infiniteScroll.page = 0;
	vars.infiniteScroll.priceMin = 10;
	vars.infiniteScroll.priceMax = 200;
	try{
		var pricesStr = $('#ex2').slider('getValue').val();
		if(pricesStr != ''){
			var prices = pricesStr.split(',');
			vars.infiniteScroll.priceMin = prices[0];
			vars.infiniteScroll.priceMax = prices[1];
		}
	}catch(e){}
	vars.infiniteScroll.platnosc = $('#radioPlatnosc1').prop('checked')?1:0;
	vars.infiniteScroll.dostawa = $('#radioDostawa1').prop('checked')?1:0;
	vars.infiniteScroll.dzieci = $('#radioDzieci1').prop('checked')?1:0;
	vars.infiniteScroll.zwierzeta = $('#radioZwierzeta1').prop('checked')?1:0;
	vars.infiniteScroll.subcategories = [];
	if(vars.infiniteScroll.isCategoryPage){
		$('.subcategory-option.active').each(function(){
			vars.infiniteScroll.subcategories.push($(this).data('category'));
		});
		if(vars.infiniteScroll.subcategories.length == 0){
			$('.subcategory-option').each(function(){
				vars.infiniteScroll.subcategories.push($(this).data('category'));
			});
		}
	};
	loadPlacesPage();
}
function loadPlacesPage(){
	$.ajax({
		method: 'POST',
		url: "loadmore.php",
		data:{
			page: vars.infiniteScroll.page,
			sortBy: vars.infiniteScroll.sortBy,
			priceMin: vars.infiniteScroll.priceMin,
			priceMax: vars.infiniteScroll.priceMax,
			paymentByCard: vars.infiniteScroll.platnosc,
			shipment: vars.infiniteScroll.dostawa,
			kids: vars.infiniteScroll.dzieci,
			animals: vars.infiniteScroll.zwierzeta,
			type: vars.infiniteScroll.type,
			when: vars.infiniteScroll.when,
			category: vars.infiniteScroll.category,
			subcategories: vars.infiniteScroll.subcategories,
			locationAddress: vars.infiniteScroll.location.address,
			locationLat: vars.infiniteScroll.location.lat,
			locationLon: vars.infiniteScroll.location.lon,
			query: vars.infiniteScroll.query
		},
		success: function(html){
			if($.trim(html)){
				$("#promos-container").append(html);
				$('div#loadmoreajaxloader').hide();
				fillJakDojadeDistances();
			}else{
				$('div#loadmoreajaxloader').html('<center>Nie ma więcej obiektów do pokazania.</center>');
				vars.infiniteScroll.isContent = false;
			}
		}
	});
}

function fillJakDojadeDistances(){
	if(vars.infiniteScroll.location.lat==0 && vars.infiniteScroll.location.lon==0) return;
	var lat1 = vars.infiniteScroll.location.lat;
	var lon1 = vars.infiniteScroll.location.lon;
	$('.jak-dojade').each(function(){
		$(this).find('span').html(getDistance(lat1, lon1, $(this).data('lat'), $(this).data('lon')));
	});
	$('.jak-dojade-mob').each(function(){
		$(this).find('span').html(getDistance(lat1, lon1, $(this).data('lat'), $(this).data('lon')));
	});
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function initializeGMap() {
	vars.geocoder = new google.maps.Geocoder();
}

google.maps.event.addDomListener(window, 'load', initializeGMap);

function pastePosition(position){
	vars.infiniteScroll.location.lat = position.coords.latitude;
	vars.infiniteScroll.location.lon = position.coords.longitude;
	var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	vars.geocoder.geocode({'latLng': latlng}, function(results, status) { 
		if (status == google.maps.GeocoderStatus.OK) {
			if (results[0]) {
				$('#homepage-location').val(results[0].formatted_address); 
				$('.topLocation').val(results[0].formatted_address); 
				vars.infiniteScroll.location.address = results[0].formatted_address;
				reloadPlaces();
			} else {
				alert('Użądzenie nie pozyskało geolokalizacji.');
			}
		} else {
			alert('Geolokalizacja nie działa z powodu: ' + status);
		}
		$('.btn-localize').css({background: '#EF4036 url(images/location_mark_white.png) center center no-repeat'});
		$('.btn-localize-top').css({background: '#FFF url(images/assets/location_mark_input.png) center center no-repeat'});
	});
}

/*--- Filtres Range Slider---*/
 $("#ex2").slider({});

/*--Slider--*/
$('.carousel').carousel({
	interval: 5000 //changes the speed
})
 
 function showApla(){
	$('#apla').fadeIn();
 }
 
 function hideModals(){
	$('#apla').fadeOut();
	$('#'+vars.openedModalId).fadeOut();
	vars.openedModalId = ''; 
	$(window).scrollTop(vars.topPositionFormodal);
	vars.topPositionFormodal = 0;
 }
 
 function showModal(modalId){
	if(vars.openedModalId){
		$('#'+vars.openedModalId).fadeOut();
		vars.openedModalId = '';
	}
	showApla();
	vars.openedModalId = modalId;
	$('#'+vars.openedModalId).fadeIn();
 }