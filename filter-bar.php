<section class="filter-box mobile-none">
	<div class="container">
		<div class="row categories">
			<div class="col-lg-8 col-md-9">
				<a href="#" class="filter-bar-option selected" data-sortby="distance">Najbliżej</a>
				<a href="#" class="filter-bar-option" data-sortby="popularity">Najpopularniejsze</a>
				<a href="#" class="filter-bar-option" data-sortby="rating">Najwyżej oceniane</a>
				<a href="#" id="filter-bar-card-pay" class="selected closable">Płatność kartą</a>
			</div>
			<div class="col-lg-4 col-md-3">
				<a class="filter pull-right">Filtruj <img src="images/assets/arrow_filter_down.png"  alt="arrow" id="bg3"  /></a>
			</div>
		</div>
	</div>
</section>