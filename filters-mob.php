<?php include("begin.php"); ?>
    <div id="lg-mobile">
        <div class="container-fluid">
            <div class="row"> 
				<div class="header-mobile"> 
					<div class="col-lg-2 col-md-2 col-xs-2 "><a href="javascript:window.history.back();"><img src="images/assets/reg_back.png" height="50" border="0" /></a></div>
					<div class="col-lg-8 col-md-8 col-xs-8 center header-mobile-title">Dodaj filtry</div>
				</div> 
            </div>
        </div>
        <div id="modal-profile-settings" class="container">
			<div class="break"></div>
			<div class="row">
				<div class="col-lg-12"><p>Średnia cena dania głównego:</p> </div> 
			</div>
			<div class="row">
			
					<div class="col-xs-6">
						od: &nbsp;&nbsp;
						<div class="filtr select-reg">							
							<select>
								<?php for($i=1; $i<=20; $i++){ ?>
									<option><?php echo $i*10; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-xs-6">
						do: &nbsp;&nbsp;
						<div class="filtr select-reg">						
						<select>
							<?php for($i=1; $i<=20; $i++){ ?>
								<option><?php echo $i*10; ?></option>
							<?php } ?>
						</select>
						</div>
					</div>
				</div>
			<div class="break"></div>
			<div class="break"></div>
			<div class="row">
				<div class="col-xs-6"><p>Płatność Kartą:</p></div> 
				<div class="col-xs-6"> 
					<input type="radio" id="radioPlatnosc2" name="platnosc" value="false">
					<label for="radioPlatnosc2" class="radious-l">nie</label>
					<input type="radio" id="radioPlatnosc1" name="platnosc" value="true" checked >
					<label for="radioPlatnosc1" class="radious-r">tak</label>					
				</div>
			</div>
			<div class="break"></div>
			<div class="row">
				<div class="col-xs-6"><p>Dostawa na telefon:</p></div> 
				<div class="col-xs-6"> 
					<input type="radio" id="radioDostawa2" name="dostawa" value="false" >
					<label for="radioDostawa2" class="radious-l">nie</label>
					<input type="radio" id="radioDostawa1" name="dostawa" value="true" checked >
					<label for="radioDostawa1" class="radious-r">tak</label>					
				</div>
			</div>
			<div class="break"></div>
			<div class="row">
				<div class="col-xs-6"><p>Przyjazne dzieciom:</p></div> 
				<div class="col-xs-6"> 
					<input type="radio" id="radioDzieci2" name="dzieci" value="false" checked>
					<label for="radioDzieci2" class="radious-l">nie</label>
					<input type="radio" id="radioDzieci1" name="dzieci" value="true">
					<label for="radioDzieci1" class="radious-r">tak</label>					
				</div>
			</div>
			<div class="break"></div>
			<div class="row">
				<div class="col-xs-6"><p>Przyjazne zwierzętom:</p></div> 
				<div class="col-xs-6" > 					
					<input type="radio" id="radioZwierzeta2" name="zwierzeta" value="false" checked>
					<label for="radioZwierzeta2" class="radious-l">nie</label>
					<input type="radio" id="radioZwierzeta1" name="zwierzeta" value="true">
					<label for="radioZwierzeta1" class="radious-r">tak</label>
				</div>
			</div>
			<div class="break"></div>
			<div class="row">
				<div class="col-xs-12 center" > 	
					<button class="button-blue">zatwierdź</button>
				</div>
			</div>
			<div class="break"></div>
        </div>
    </div>
<?php include("end.php"); ?>
