<?php include("header.php"); ?>

<section class="container breadcrumb">
	<ul>
		<li><a href="/">Strona główna</a></li> 
		<li>Regulamin</li>
	</ul>
</section>

<section class="container regulations">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1 style="margin-top:10px;">Regulamin</h1>
	<p>
		<span class="bold">1. Definicje.</span><br/>
		Ilekroć w niniejszym dokumencie jest mowa o:<br/>
		1.1. „Usłudze” należy przez to rozumieć usługi realizowane w ramach PlatformyKołomnie.pl dla Użytkowników Platformy (dalej „Użytkownicy”, pojedynczo „Użytkownik”).<br/>
		1.2. „Platforma”należy przez to rozumieć Serwis informacyjno- geolokalizacyjny Kołomnie.pl udostępniony przez Wydawcę w formie on-line pod adresem internetowym www.kolomnie.pl, wraz z wszelkimi dostępnymi funkcjami.<br/>
		1.3. „Użytkowniku Serwisu”należy przez to rozumieć osobę fizyczną lub prawną albo jednostkę organizacyjną niebędąca osobą prawną, której ustawa przyznaje zdolność prawną, podejmującą czynności zmierzające do korzystania z Platformy lub korzystającą z Platformy.<br/>
		1.4. „Danych osobowych”należy przez to rozumieć wszelkie dane i informacje, w tym zdjęcia, nagrania audio i wideo oraz dane osobowe, zdefiniowane w ustawie z dnia 29 sierpnia 1997 o ochronie danych osobowych. W rozumieniu ustawy za dane osobowe uważa się wszelkie informacje dotyczące zidentyfikowanej lub możliwej do zidentyfikowania osoby fizycznej.
		<br/><br/>
		<span class="bold">2.	Postanowienia ogólne.</span><br/>
		2.1. PlatformaKolomnie.pl przykłada szczególną wagę do ochrony prywatności swoich Użytkowników. <br/>
		2.2. PlatformaKolomnie.pl starannie dobiera i stosuje odpowiednie środki techniczne i organizacyjne przewidziane prawem a zapewniające ochronę przetwarzanych danych.<br/>
		2.3. PlatformaKolomnie.pl zabezpiecza dane przed ich udostępnieniem osobom nieupoważnionym, jak również przed ich przetwarzaniem z naruszeniem obowiązujących przepisów prawa.<br/>
		2.4. PlatformaKolomnie.pl nie przetwarza i nie składuje danych osobowych. Platforma Kolomnie.pl wykorzystuje w celach rejestracji rozwiązania Serwisów Partnerskich (w szczególności platformę Facebook.com), która jest administratorem wcześniej jej powierzonych danych osobowych prze użytkownika.<br/>
		2.5. W wykorzystywaniu rozwiązań Serwisów Partnerskich, Kolomnie.pl działa zgodnie z regulaminami i licencjami współpracujących Serwisów.
		<br/><br/>
		<span class="bold">3.	Zasady korzystania z Platformy przez Użytkowników.</span><br/>
		3.1. Korzystanie z Platformy Kolomnie.pl nie wymaga podawania przez Użytkownika Danych Osobowych.<br/>
		3.2. Korzystanie z niektórych funkcjonalności Platformy Kolomnie.pl może wiązać się z koniecznością zalogowania się do Platformy przez użytkownika poprzez Serwis Partnerski (w szczególności Facebook.com). W takim przypadku niezalogowanie się do PlatformyKolomnie.pl poprzez Serwis Partnerskimoże ograniczać możliwość korzystania z Platformy.<br/>
		3.3. Logowanie się do Platformy Kolomnie.pl jest dobrowolne. <br/>
		3.4. Użytkownik ma prawo dostępu do treści swoich danych przetrzymywanych i przetwarzanych poprzez Serwisy Partnerskie (Serwisy Partnerskie) oraz ich poprawiania wewnątrz tych serwisów.<br/>
		3.5. Zasady korzystania z poszczególnych Usług Serwisów Partnerskich określone są w ramach regulaminów tychże Serwisów.<br/>
		3.6. Dane Użytkowników mogą być udostępniane podmiotom upoważnionym do ich otrzymywania na podstawie przepisów prawa w szczególności organom wymiaru sprawiedliwości, administracji fiskalnej.
		<br/><br/>
		<span class="bold">4.	Podstawa przetwarzania Danych Osobowych.</span><br/>
		4.1. Dane Osobowe przetwarzane są na podstawie zgody wcześniej wyrażanej przez Użytkownika w Serwisach Partnerskich Platformy Kolomnie.pl i tylko poprzez te Serwisy.<br/>
		4.2. Wyrażenie przez Użytkownika zgody na przetwarzanie jego Danych Osobowych nastąpiło przez zaznaczenie właściwego miejsca w formularzu w trakcie procedury rejestracyjnej lub innej procedury Serwisu Partnerskiego.<br/> 
		4.3. Użytkownik może w każdej chwili odwołać zgodę w Serwisie Partnerskim.
		<br/><br/>
		<span class="bold">5.	Dane związane z przeglądaniem zawartości Serwisu.</span><br/>
		5.1. Platforma Kolomnie.pl zbiera informacje związane z przeglądaniem zawartości Serwisu przez Użytkowników, takie jak liczba i źródło wizyt w danym Serwisie, czas wizyty, przeglądane treści, liczba i rodzaj otwieranych podstron, wykorzystane odesłania, czy numer IP komputera. Portal nie łączy takich informacji z Danymi Osobowymi Użytkownika i nie wykorzystuje ich do identyfikacji Użytkownika, chyba że jest to niezbędne dla prawidłowego świadczenia Usługi.<br/>
		5.2. Platforma Kolomnie.pl wykorzystuje informacje, o których mowa w ust. 5.1. wyłącznie w celach związanych z badaniem rynku i ruchu internetowego w ramach Platformy, w celach statystycznych, w szczególności dla oceny zainteresowania treściami zamieszczanymi w Platformie oraz ulepszaniem zawartości Platformy i jakości świadczonych Usług.<br/>
		5.3. Platforma Kolomnie.pl nie stosuje plików typu cookie lub podobnych. 
		<br/><br/>
		<span class="bold">6.	Kontakt.</span><br/>
		6.1. Adres, pod którym można się skontaktować Portalem Kolomnie.pl:<br/>
		Kolomnie.pl<br/>
		Pion: NMI Group AIP SGH<br/>
		Akademickie Inkubatory Przedsiębiorczości<br/>
		ul. Piękna 68, II p.<br/>
		00-672 Warszawa<br/>
		Tel. 500.131.645<br/>
		E-mail: kolomnie@kolomnie.pl
    </p>
		</div> 	
	</div>	
</section>

<?php include("footer.php"); ?>

