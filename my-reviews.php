<?php include("header.php"); ?>

<section class="container breadcrumb">
	<ul>
		<li><a href="/">Strona główna</a></li>
		<li>Moje recenzje</li>
	</ul>
</section>

<div class="divider"></div>

<section class="container my-reviews">
	<div class="row">
		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 center ">
			<div class="face center" style="background:url(images/fake-face2.png) center center no-repeat;background-size:cover;"></div>
			<div class="text">Antek Wróblewski</div>
		</div>
	 
		<div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
			<div class="review">
				<div class="title">					
					<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 text1"><div class="pull-left">Recenzja dla: Cafe Biba</div>
						<div class="small-stars small-stars-1 pull-left"></div></div>
						<div class="date col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-left">Dzisiaj, 20:45</div>
					</div>
				<div class="clearfix"></div>
				<div class="col-lg-12">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa.
				</div>
			</div>
			<!--myReviews-->
			<div class="review">
				<div class="title">					
					<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 text1"><div class="pull-left">Recenzja dla: Cafe Biba</div>
						<div class="small-stars small-stars-1 pull-left"></div></div>
						<div class="date col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-left">Dzisiaj, 20:45</div>
					</div>
				<div class="clearfix"></div>
				<div class="col-lg-12 ">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa.
				</div>
			</div>
			<!--myReviews-->
			<div class="review">
				<div class="title">					
					<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 text1"><div class="pull-left">Recenzja dla: Cafe Biba</div>
						<div class="small-stars small-stars-1 pull-left"></div></div>
						<div class="date col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-left">Dzisiaj, 20:45</div>
					</div>
				<div class="clearfix"></div>
				<div class="col-lg-12">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa.
				</div>
			</div>
			<!--myReviews-->
			<div class="review">
				<div class="title">					
					<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 text1"><div class="pull-left">Recenzja dla: Cafe Biba</div>
						<div class="small-stars small-stars-1 pull-left"></div></div>
						<div class="date col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-left">Dzisiaj, 20:45</div>
					</div>
				<div class="clearfix"></div>
				<div class="col-lg-12">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa.
				</div>
			</div>
			<!--myReviews-->
			<div class="review">
				<div class="title">					
					<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 text1"><div class="pull-left">Recenzja dla: Cafe Biba</div>
						<div class="small-stars small-stars-1 pull-left"></div></div>
						<div class="date col-lg-3 col-md-4 col-sm-4 col-xs-12 pull-left">Dzisiaj, 20:45</div>
					</div>
				<div class="clearfix"></div>
				<div class="col-lg-12">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa.
				</div>
			</div>		 
		</div>
	</div>
</section>
			
<?php include("footer.php"); ?>

