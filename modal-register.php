<div id="modal-register" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Rejestracja</h1>
		<div class="required-fields">
			<span class="required">*</span> pola obowiązkowe
		</div>
		<form id="form-register" action="/" method="post">
			<input type="email" id="register-email" name="registerEmail" placeholder="E-mail" required /> <span class="required">*</span>
			<input id="register-password" name="registerPassword" placeholder="Hasło" required type="password" /> <span class="required">*</span>
			<div class="sex">
				<input type="radio" name="registerSex" id="register-female" value="female" required /> <label for="register-female">Kobieta</label>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="registerSex" id="register-male" value="male" required /> <label for="register-male">Mężczyzna</label>
				<span class="required">*</span>
			</div>
			<div id="register-errors"></div>
			<div class="info">
				Poznajmy się lepiej, abyś otrzymywał informacje, które Cię interesują:
			</div>
			<input type="text" id="register-firstname" name="registerFirstname" placeholder="Imię"/>
			<input type="text" id="register-lastname" name="registerLastname" placeholder="Nazwisko"/>
			<div class="dob">
				Data urodzenia:
			</div>
			<select id="register-dob-day" name="registerDobDay">
				<option>DD</option>
				<?php for($i=1; $i<=31; $i++){ echo "<option>".$i."</option>"; } ?>
			</select>
			<select id="register-dob-month" name="registerDobMonth">
				<option>MM</option>
				<?php for($i=1; $i<=12; $i++){ echo "<option>".$i."</option>"; } ?>
			</select>
			<select id="register-dob-year" name="registerDobYear">
				<option>RRRR</option>
				<?php for($i=1920; $i<=date("Y"); $i++){ echo "<option>".$i."</option>"; } ?>
			</select>
			<button id="register-submit" class="button-red">Zakładam konto</button>
		</form>
	</div>
</div>