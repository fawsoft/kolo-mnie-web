<div id="modal-change-password-request" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Zmień hasło</h1>
		<div>Podaj adres email, a prześlemy link do zmiany hasła.</div> 
		<input type="email" id="change-password-email" name="changePasswordEmail" placeholder="E-mail" required />
		<button class="button-red">Zatwierdź</button>
	</div>
</div>