<?php include("begin.php"); ?>
    <div id="lg-mobile">
        <div class="container-fluid">
            <div class="row">
                <div class="header-mobile"> 
					<div class="col-lg-2 col-md-2 col-xs-2 "><a href="javascript:window.history.back();"><img src="images/assets/reg_back.png" height="50" border="0" /></a></div>
					<div class="col-lg-8 col-md-8 col-xs-8 center header-mobile-title">Kontakt</div>
				</div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4"> 
                    <div class="contact-mob">
						Kolomnie Sp. z o.o.
						<br />
						ul. Marszałkowska 49 m. 12
						<br />
						00-230 Warszawa
						<br />
						NIP: 596-5-5435-5,
						REGON: 43424342
						<br />
						KRS: 04400404
						<br />
						<br />
						<a href="mailto:biuro@kolomnie.pl" class="link-blue">biuro@kolomnie.pl</a>
						<br /> 
						<big>Janek: +48 660 586 289</big>
						<br />
						<br />
						<big>Łukasz: +48 660 586 280</big>
						<br />
						<br />
					</div>
                </div>
            </div>
        </div>
    </div>
<?php include("end.php"); ?>
