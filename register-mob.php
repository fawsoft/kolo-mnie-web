<?php include("begin.php"); ?>
    <div id="lg-mobile">
        <div class="container-fluid">
            <div class="row">
                <div class="header-mob register">
                        <div class="col-lg-2 col-md-2 col-xs-2 "><a href="javascript:window.history.back();" class="register-back"></a> </div>
                        <div class="col-lg-10 col-md-10 col-xs-10">Rejestracja </div>   
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-4 col-lg-4">
                    <form action="/" id="mobile-log" class="register-mob-form" method="post">
                        <p>Załóż konto</p>
                        <div>
                            <input id="register-mob-email" type="email" class="form-control"  placeholder="E-mail *">
                        </div>
                        <div>
                            <input id="register-mob-password" type="password" class="form-control"  placeholder="Hasło *">
                        </div>
                        <p>Poznajmy się lepiej, abyś otrzymywał informacje, które Cię interesują:</p>
                        <div>
                            <input type="text" class="form-control"  placeholder="Imię">
                        </div>
                        <div>
                            <input type="text" class="form-control"  placeholder="Nazwisko">
                        </div>
                        <div class="select-reg">
                            <select class="form-control">    
                                <option value="">Płeć</option>
                                <option value="male">Mężczyzna</option>
                                <option value="female">Kobieta</option>
                            </select>   
                        </div>
                          <p>Data urodzenia</p>
                           <div class="data-ur">                        
                                   
                                <div class="col-lg-3 col-md-4 col-xs-6">
                                    <div class="select-reg">
                                        <select id="register-dob-day" class="form-control">
                                            <option>DD</option>
                                            <?php for($i=1; $i<=31; $i++){ echo "<option>".$i."</option>"; } ?>
                                        </select>  
                                    </div>
                                </div>
                               <div class="col-lg-3 col-md-4 col-xs-6">
                                    <div class="select-reg">
                                        <select id="register-dob-month" class="form-control">
                                            <option>MM</option>
                                            <?php for($i=1; $i<=12; $i++){ echo "<option>".$i."</option>"; } ?>
                                        </select>
                                    </div>                                
                               </div>
                               <div class="col-lg-6 col-md-4 col-xs-12">
                                    <div class="select-reg">
                                        <select id="register-dob-year" class="form-control">
                                            <option>RRRR</option>
                                            <?php for($i=1920; $i<=date("Y"); $i++){ echo "<option>".$i."</option>"; } ?>
                                        </select>
                                    </div>                                
                               </div>
                           </div> 
                           <p>* pola obowiązkowe<p>
                        <div>
                            
                            <button id="register-mob-submit" type="button" class="btn btn-red">Zakładam konto</button>  
                        </div>
                        <div>
                            
                        </div>      
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include("end.php"); ?>
