<div id="modal-inform-friends" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Powiadom znajomych</h1>
		<div class="text">
			Dzięki za pomoc! Twoje zgłoszenie zostało przyjęte.
		</div>
		<button class="button-red">Podziel się informacją ze znajomymi</button>
	</div>
</div>