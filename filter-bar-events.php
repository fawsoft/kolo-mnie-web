<section class="filter-box filter-barEvents">
	<div class="container">
		<div class="row categories">
			<div class="col-lg-8">
				<a href="#" class="filter-bar-events-option selected" data-when="today">Dzisiaj</a>
				<a href="#" class="filter-bar-events-option" data-when="tomorrow">Jutro</a>
				<a href="#" class="filter-bar-events-option" data-when="weekend">Weekend</a>
				<a id="filter-bar-events-date" href="#" class="filter-bar-events-option" data-when="calendar">Inny termin</a>
				<div id="filter-bar-events-calendar"></div>
			</div>
		</div>
	</div>
</section>
