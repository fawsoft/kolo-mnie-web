<?php include("header.php"); ?>
<section class="container breadcrumb mobile-none">
	<ul>
		<li><a href="/">Strona główna</a></li>
		<li><a href="/">Wyszukiwanie</a></li>
		<li>Szczegóły miejsca</li>
	</ul>
</section>
<section class="place-header" >
   <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">			
        <!-- Carousel indicators -->
 		<ol class="carousel-indicators mobile-none">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol> 
        <!-- Carousel items -->
        <div class="carousel-inner">
        	<div class="content-in container-fluid" style="background:url(images/event-gradient.png) repeat-x; height:100%;; background-position:bottom; position:absolute; bottom: 0px; width: 100%; left:0px; right:0px;">
        		<div class="container">         
	        		<div class="row"><!--faces-->
	        			<div class="col-lg-6 col-md-12 col-xs-12 mobile-show" >        				 
	        				 <div class="pull-left">
								<div class="platform facebook"></div>
								<div class="platform instagram"></div>
								<div class="platform yelp"></div>
								<!-- facebook, yelp, instagram -->	
	        				 </div>
	        				 <div class="faces pull-right">
								<div class="face" style="background:url(images/fake-face1.png) center center no-repeat; background-size:cover;"></div>
								<div class="face" style="background:url(images/fake-face2.png) center center no-repeat; background-size:cover;"></div>
								<div class="amount">+50</div>
							</div>
	        			</div>
	        			<div style="position:absolute; bottom: 20px;  width:90%; left:5%;" >        				 
	        				<div class="avatar-box">
	        						<div class="vote-star mobile-none">	<div id="stars-rate" class="stars stars-0 ">
								<div class="rate" data-rate="1"></div>
								<div class="rate" data-rate="2"></div>
								<div class="rate" data-rate="3"></div>
								<div class="rate" data-rate="4"></div>
								<div class="rate" data-rate="5"></div>
							</div></div>
	        						<div class="label-prom pull-right mobile-none">PROMOCJA</div>
		        				<div class="col-lg-12 col-md-3 col-sm-2 col-xs-3">
		        					<div class="avatar"><img src="images/avatar.png" alt=""></div>
		        					

		        				</div>
		        				<div class="col-lg-12 col-md-12 col-sm-9 col-xs-9">
		        					<div class="row">
										<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				        					<h1>Trattoria Toscana - Kuchnia Śródziemnomorska</h1>
				        					<p class="adress">Warszawa, ul. Al. Stanów Zjednoczonych</p>
		        						</div>
		        						<div class="col-lg-6 col-md-12 col-sm-9 col-xs-9 mobile-none">
			        						<div class="like-event">
												<div class="heart"></div>
												<div class="text">Polub</div>
											</div>
		        						</div>
		        					</div>		        				
		        				</div>
	        				</div>
	        			</div>      		 
	        		</div>
	        	</div>			
			</div>
            <div class="item active"><img src="http://lorempixel.com/1800/600/?rnd=<?php echo rand(); ?>"></div>
            <div class="item" ><img src="http://lorempixel.com/1800/600/?rnd=<?php echo rand(); ?>"></div>
            <div class="item"><img src="http://lorempixel.com/1800/600/?rnd=<?php echo rand(); ?>"></div>
        </div> 
    </div>
</section>
<div class="container place-description">
<div class="row">
	<div class="col-lg-7">		
		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="close-open open">Otwarte</div><!-- open, closed -->
				<div class="description">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget sagittis mauris, id congue nulla. Donec vulputate sem mauris, et tincidunt dui faucibus vitae. Sed sed maximus velit, at pretium elit. Phasellus sed accumsan ante. Aenean eget justo facilisis, tristique diam vel, accumsan massa. Etiam vehicula velit a libero convallis, a aliquam lacus vehicula. Quisque malesuada fermentum turpis, eget maximus lectus vulputate at. Etiam vitae ultrices diam, a sodales sem. Proin auctor ligula quis metus condimentum, ac tempor neque imperdiet. Donec sit amet tellus neque. Fusce ultricies metus ut nulla malesuada tincidunt. Nullam quis velit ut neque sagittis convallis ac a mi. Proin turpis metus, faucibus ullamcorper sollicitudin malesuada, vestibulum in justo. Maecenas et orci sed mi fringilla aliquet. Morbi eget sapien a mauris consectetur scelerisque vitae at urna. Proin et est tincidunt, rutrum magna in, eleifend tortor.
				</div> 
			<div id="place-more">Więcej</div>
		</div>

		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="options">
				<div class="col-lg-6 col-md-6 col-xs-6"><div class="place-option card active">płatność kartą</div></div>
				<div class="col-lg-6 col-md-6 col-xs-6"><div class="place-option animal active">przyjazne zwierzętom</div></div>
				<div class="col-lg-6 col-md-6 col-xs-6"><div class="place-option kids active">przyjazne dzieciom</div></div>
				<div class="col-lg-6 col-md-6 col-xs-6"><div class="place-option phone">jedzenie na telefon</div></div>
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-xs-12">	
				<div class="place-open">
				Czynne:
				<br />
				Pon. - Pt.: 10:00 - 22:00
				<br />
				Sob. - Niedz.: 11:00 - 22:00
				<br />
				tel. (22) 567 10 98
				<br />				
				średnia cena dania głównego: powyżej 50 zł
				<a href="#" class="report" data-modal="modal-report-poi">Zgłoś</a>
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-xs-12">
			<div class="links">				
				<?php
					function skroc($tekst, $ilosc_znakow) { //SKRACA CIĄG DO PODANEJ DŁUGOŚCI
					return substr($tekst, 0, $ilosc_znakow).'...';
					} 
				?>	
				<a href="#" class="trackback"><?php echo(skroc("http://www.ticketpro.pl/jnp/muzyka/42344543-nowe-sytuacje-group.html", 40)."<br>"); ?></a>
				<a href="#" class="facebook"><?php echo(skroc("http://www.ticketpro.pl/jnp/muzyka/42344543-nowe-sytuacje-group.html", 40)."<br>"); ?></a>	
			</div>
		</div>
			<div class="col-lg-12 col-md-12 col-xs-12">
				<div class="order-food">
					<button class="btn btn-red btn-fix btn-order-food" type="button" data-link="http://www.foodpanda.com">zamów jedzenie</button>
				</div>
			</div>		
		<div class="col-lg-12 col-md-12 col-xs-12">
 			<div class="row">
 				<div class="photo">
					<h2>Zdjęcia</h2>				
					<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-1.png" border="0" /></a></div>
					<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-2.png" border="0" /></a></div>
					<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-3.png" border="0" /></a></div>
					<div class="col-lg-3 col-md-3 col-xs-3"><a href="http://lorempixel.com/800/600/?rnd=<?php echo rand(); ?>" data-lightbox="place"><img src="images/place-photo-4.png" border="0" /></a></div>
				</div>
			</div>				
		</div>
		<div class="col-lg-12 col-md-12 col-xs-12">
			<section id="comment">
				<div class="row vote">
					<div class="col-lg-6">
						<h2><i></i> Opinie (24)</h2>
					</div>
					<div class="col-lg-6">
						<div class="ocena pull-right mobile-none">
							<p>Oceń miejsce</p>
							<div id="stars-rate" class="stars stars-0 ">
								<div class="rate" data-rate="1"></div>
								<div class="rate" data-rate="2"></div>
								<div class="rate" data-rate="3"></div>
								<div class="rate" data-rate="4"></div>
								<div class="rate" data-rate="5"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<input id="placeCommentContent" type="text" class="comment" placeholder="Wpisz treść komentarza">
						<button id="placeCommentSubmit" type="submit" class=" btn btn-red submitComment"> Wyślij</button>
					</div>					
				</div>
				<div class="row comment-item">
					<div class="col-lg-1 col-md-1 col-xs-2 comments-avatar"><img src="images/fake-face1.png" alt="avatar"></div>
					<div class="col-lg-11 col-md-11 col-xs-10 comment-desc">
						<div class="col-lg-12">
						<div class="CommentName">Marcin PacMan</div><div class="comment-vote"></div><div class="comment-date pull-right">20:22 DZISIAJ</div>
						<p>Tresc komentarza polecam ta knajpe maja super spaghetti wykończone w wysokim standardzie położone na terenie strzeżonym budynku przynależne miejsce parkingowe  wykończone w wysokim standardzie położone</p></div>
					</div>
				</div>
				<div class="row comment-item">
					<div class="col-lg-1 col-md-1 col-xs-2 comments-avatar"><img src="images/fake-face2.png" alt="avatar"></div>
					<div class="col-lg-11 col-md-11 col-xs-10 comment-desc">
					<div class="col-lg-12">
						<div class="CommentName">Marcin PacMan</div><div class="comment-vote"></div><div class="comment-date pull-right">20:22 DZISIAJ</div>
						<p>Tresc komentarza polecam ta knajpe maja super spaghetti wykończone w wysokim standardzie położone na terenie strzeżonym budynku przynależne miejsce parkingowe  wykończone w wysokim standardzie położone</p></div>
					</div>
					</div>
				<div class="row comment-item">
					<div class="col-lg-1 col-md-1 col-xs-2 comments-avatar"><img src="images/fake-face1.png" alt="avatar"></div>
					<div class="col-lg-11 col-md-11 col-xs-10 comment-desc">
						<div class="col-lg-12">
						<div class="CommentName">Marcin PacMan</div><div class="comment-vote"></div><div class="comment-date pull-right">20:22 DZISIAJ</div>
						<p>Tresc komentarza polecam ta knajpe maja super spaghetti wykończone w wysokim standardzie położone na terenie strzeżonym budynku przynależne miejsce parkingowe  wykończone w wysokim standardzie położone</p></div>
					</div>
				</div>				 
			</section>
		</div>		
	</div>
		<div class="col-lg-5 col-md-12 col-xs-12">
			 <div class="col-lg-12 mobile-none">
				<div id="behere">
					<div class="col-lg-5"><p>Ty i 10 znajomych tu było W sumie 234 osoby</p> </div>
					<div class="col-lg-7">
						<img src="images/fake-face1.png" alt="avatar">
						<img src="images/fake-face2.png" alt="avatar">
						<img src="images/fake-face1.png" alt="avatar">
						<img src="images/fake-face2.png" alt="avatar">
						<div class="behereNumber">+10</div>
					</div>
				</div>
			 </div>

			 <div class="col-lg-12 share">
				<p>Poleć nas</p>
				<a href="#" class="share-facebook fb" data-desc="Trattoria Toscana - Kuchnia Śródziemnomorska" data-img="http://kolomnie.farandwide.pl/images/fake.jpg" data-url="http://kolomnie.farandwide.pl"></a>
				<a href="#" class="share-twitter tw" data-url="http://kolomnie.farandwide.pl" data-desc="Trattoria Toscana - Kuchnia Śródziemnomorska"></a>
				<a href="#" class="share-google gp" data-url="http://kolomnie.farandwide.pl"></a>
				<a href="#" class="mail" data-modal="modal-invite"></a>
			 </div>

			 <div class="col-lg-12 mobile-none">
				<h2>Zobacz na mapie</h2>
				<div class="expand-map-div">
					<div class="expand-map" data-lat="52.266601691311" data-lon="20.977800833282"></div>
				</div>
				<div class="local-map" id="local-map"></div>
				<script>
					var localMap = null;
					var bounds = new google.maps.LatLngBounds();
					function initializeLocalMap() {
						var mapOptions = {
							center: { lat: -34.397, lng: 150.644},
							zoom: 8
						};
						localMap = new google.maps.Map(document.getElementById('local-map'), mapOptions);
						var myLatlng = new google.maps.LatLng(52.266601691311, 20.977800833282);
						var marker = new google.maps.Marker({
							position: myLatlng,
							map: localMap,
							title: 'Lorem ipsum',
							icon: 'images/lokalizacja_ikona.png'
						});
						bounds.extend(marker.position);
						localMap.fitBounds(bounds);
						getLocation();
					}
					function addUserMarkerForLocalMap(position){ 
						vars.infiniteScroll.location.lat = position.coords.latitude;
						vars.infiniteScroll.location.lon = position.coords.longitude;
						var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
						var marker = new google.maps.Marker({
							position: myLatlng,
							map: localMap,
							title: 'Twoje położenie',
							icon: 'images/user_position_icon.png'
						});
						bounds.extend(marker.position);
						localMap.fitBounds(bounds);
					}
					function getLocation() {
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(addUserMarkerForLocalMap);
						}
					}
					google.maps.event.addDomListener(window, 'load', initializeLocalMap);
				</script>
			 </div>
			 <div class="col-lg-12 mobile-none">
				<h2>Tagi</h2>				
				<div class="place-tags tags">
					<a href="#">Piaskownica</a>
					<a href="#">Makaron z sosem</a>
					<a href="#">Solone śledzie</a>
					<a href="#">śledzie</a>
					<a href="#">Solo na ostro</a>
					<a href="#">Pralka</a>
					<a href="#">Bojler na ciepłą wode</a>
					<a href="#">Solone</a>
					<a href="#">Bulgotnica do WSKi</a>
					<a href="#">Solone</a>
					<a href="#">Solo na ostro</a>
				</div>
			 </div>				
			<div class="col-lg-12 mobile-none">
				<h2>Co nowego na Facebooku</h2>				
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&appId=828532820530910&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like-box" data-href="https://www.facebook.com/MultikinoPolska?fref=ts" data-width="430" data-stream="true" data-show-border="false"  data-header="false" data-show-faces="false"></div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>	
		<div class="mobile-none">
			<h2>Podobne Lokale</h2>	
			<?php include("place-box.php"); ?>
			<?php include("place-box.php"); ?>
			<?php include("place-box.php"); ?> 
		</div> 
	</div>			 
		<script>
			$(document).ready(function() {
				var descriptionHeight = $('.place-description .description').height();
				if(descriptionHeight >= 115){
					$('.place-description .description').css({height: '115px'});
					$('#place-more').show();
				}				
				$('#place-more').click(function(){
					$('.place-description .description').css({height: 'auto'}); 
					$('#place-more').hide();
				});
			});
			</script> 
		</div>
	</div>
 
<?php include("footer.php"); ?>

