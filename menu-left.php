 
<nav id="cd-lateral-nav">		 
		<ul id="ulMenu">		 
		<li><div class="mobile-none"><a href="categories.php"><i class="cat"></i>KATEGORIE</a></div>
		<div class="mobile-show"><a href="categories-mob.php"><i class="cat"></i>KATEGORIE</a></div></li>
		<li><a href="promos.php"><i class="pro"><span class="booble">21</span></i>PROMOCJE</a></li>
		<li><a href="news.php"><i class="news"></i>NOWOŚCI</a></li>
		<li><a href="events.php"><i class="event"></i>WYDARZENIA</a></li>
		<li class="item-has-children mobile-show"> 
		<a href="#0"><i class="changecity"></i>ZMIEŃ MIASTO</a>
			<ul class="sub-menu">
				<li><a href="#0">Warszawa</a></li>
				<li><a href="#0">Kraków</a></li>
				<li><a href="#0">Katowice</a></li>
				<li><a href="#0">Górnośląski Okręg</a></li>
			</ul>
		</li> <!-- item-has-children -->
<!-- 	<li class="mobile-show"><a href="change-city-mob.php"><i></i>ZMIEŃ MIASTO</a></li> -->
		<li class="yellow"><a href="my-reviews.php"><i class="rec"><span class="booble">12</span></i>MOJE RECENZJE</a></li>
		<li class="yellow"><a href="friends-favorite.php"><i class="like"></i>ZNAJOMI POLUBILI</a></li>
		<li class="yellow"><a href="favorite.php"><i class="mostlike"></i>ULUBIONE</a></li>		
		<?php if($loggedIn){ ?>		
		<li class="item-has-children blue mobile-show"> 
		<a href="#0"><i class="settings"></i>USTAWIENIA</a>
		<ul class="sub-menu">
		<li><a href="filters-mob.php"><i class="filtr"></i>dodaj filtry</a></li>
		<li><a data-modal="modal-profile-settings"><i class="ustawienia"></i>sprawdz ustawienia</a></li>
		<li><a href="#"><i class="fejs"></i>połącz z facebookiem</a></li>
		<li><a data-modal="modal-logout"><i class="logout"></i>Wyloguj</a></li>
		</ul>
		</li> 
		<?php } ?>
		<li class="blue"><div class="mobile-none"><a href="#" data-modal="modal-contact"><i class="mail"></i>KONTAKT</a></div>
		<div class="mobile-show"><a href="contact-mob.php"><i class="mail"></i>KONTAKT</a></div></li>
		<li class="blue"><a href="regulations.php"><i class="reg"></i>REGULAMIN</a></li>
		<li class="blue language"> <a href="#"><i class="lang"></i>JĘZYK</a></li>

	</ul>
	 
 </nav>
