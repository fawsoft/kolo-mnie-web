<div id="place-promo">
	<div class="close"></div>
	<div class="text center">
		<div class="title">Dostępna promocja!</div>
		<div class="img" style="background:url(http://lorempixel.com/400/200/) center center no-repeat; background-size:cover;">
			<div class="promo">
				Wszystkie zestawy 
				<br />
				do 30% taniej
			</div>
		</div>
		<button class="button">Skorzystaj z promocji</button>
		<div class="remark">
			Wejdź do aplikacji,
			<br />
			aby skorzystać z promocji.
		</div>
	</div>
</div>