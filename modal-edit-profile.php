<div id="modal-edit-profile" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Edytuj profil</h1>
		<div class="required-fields">
			<span class="required">*</span> pola obowiązkowe
		</div>
		<form action="/" method="post">
			<div class="lbl">Email</div>
			<input type="email" id="edit-profile-email" name="editProfileEmail" placeholder="Email" required /> <span class="required">*</span>
			<div class="lbl">Hasło</div>
			<input id="edit-profile-password" name="editProfilePassword" placeholder="Hasło" required type="password" /> <span class="required">*</span>
			<div class="lbl">
				<a id="edit-profile-change-password" href="#" class="link">Zmień hasło</a>
			</div>
			<div class="lbl">
				<input type="radio" name="editProfileSex" id="edit-profile-female" value="female" required /> <label for="edit-profile-female">Kobieta</label>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" name="editProfileSex" id="edit-profile-male" value="male" required /> <label for="edit-profile-male">Mężczyzna</label>
				<span class="required">*</span>
			</div>
			<div id="edit-profile-errors"></div>
			<div class="info">
				Poznajmy się lepiej, abyś otrzymywał informacje, które Cię interesują:
			</div>
			<div class="lbl">Imię</div>
			<input type="text" id="edit-profile-firstname" name="editProfileFirstname" placeholder="Imię"/>
			<div class="lbl">Nazwisko</div>
			<input type="text" id="edit-profile-lastname" name="editProfileLastname" placeholder="Nazwisko"/>
			<div class="lbl">
				Data urodzenia:
			</div>
			<select id="edit-profile-dob-day" name="editProfileDobDay">
				<option>DD</option>
				<?php for($i=1; $i<=31; $i++){ echo "<option>".$i."</option>"; } ?>
			</select>
			<select id="edit-profile-dob-month" name="editProfileDobMonth">
				<option>MM</option>
				<?php for($i=1; $i<=12; $i++){ echo "<option>".$i."</option>"; } ?>
			</select>
			<select id="edit-profile-dob-year" name="editProfileDobYear">
				<option>RRRR</option>
				<?php for($i=1920; $i<=date("Y"); $i++){ echo "<option>".$i."</option>"; } ?>
			</select>
			<button id="edit-profile-submit" class="button-red">Zapisz</button>
		</form>
	</div>
</div>