<?php /*
	available variables:
	$_POST['phrase'] - value from autocomplete
	$_POST['sortBy'] - distance / popularity / rating
*/ ?>
[
<?php for($i=0; $i<10; $i++){ ?>
	{
		"name":"Lorem ipsum <?php echo $i; ?>",
		"lat":<?php echo 52+(mt_rand() / mt_getrandmax()); ?>,
		"lon":<?php echo 21+(mt_rand() / mt_getrandmax()); ?>,
		"address": "Warszawa, Gołębiowskiego 4",
		"stars":1,
		"img": "http://lorempixel.com/100/100/?rnd=<?php echo rand(); ?>"
	},
<?php } ?>
	{
		"name":"Lorem ipsum 0",
		"lat":52,
		"lon":21,
		"address": "Warszawa, Gołębiowskiego 4",
		"stars":3,
		"img": "images/fake.jpg"
	}
]