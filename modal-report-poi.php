<div id="modal-report-poi" class="modal">
	<div class="in">
		<div class="close"></div>
		<h1>Zgłoszenie nieprawidłowości punktu</h1>
		<div class="info">
			Dlaczego zgłaszasz punktu "Klub 1500 m2"?
		</div>
		<div class="reason">
			<input type="checkbox" id="report-poi-foursquare" /> <label for="report-poi-foursquare">Nie ma punktu na Foursquare</label>
		</div>
		<div class="reason">
			<input type="checkbox" id="report-poi-fb" /> <label for="report-poi-fb">Nie ma punktu na Facebooku</label>
		</div>
		<div class="reason">
			<input type="checkbox" id="report-poi-invalid" /> <label for="report-poi-invalid">Punkt jest nieaktualny</label>
		</div>
		<div class="reason">
			<input type="checkbox" id="report-poi-spam" /> <label for="report-poi-spam">To jest spam</label>
		</div>
		<button id="report-poi-submit" class="button-red">Wyślij</button>
	</div>
</div>